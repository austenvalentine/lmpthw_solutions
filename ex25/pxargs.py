import subprocess
import sys

args = sys.argv[1:]

# positional arguments stored as {option:None}
# flags stored as {flag:False}
# -I stores the string to be substituted
# -a stores the filename to be read instead of stdin
option = { "-I": None,
            "-a": None}

i = 0
xarg_args = []
subp_args = []

# parse xarg arguments
while i < len(args):
    # check if argument is start of subprocess command
    if args[i] not in option.keys():
        subp_args = args[i:]
        break
    else:
        # check if arg is positional
        if args[i] in ("-I", "-a"):
            option[args[i]] = args[i + 1]
            i += 2
        # check if arg is flag
        elif args[i] in ():
            i += 1
        else:
            # catch all. continue iteration through xarg arguments
            i += 1
            

# read standard input for pipe or input from file
if option["-a"]:
    try:
        fin = open(option["-a"])
    except:
        print("INVALID FILENAME")
else:
    fin = sys.stdin
feeds = [line.strip() for line in fin.readlines()]


# parse subprocess arguments
_I_indices = []
if len(subp_args) == 0 or subp_args[0] == option["-I"]:
    subp_args.insert(0, "/bin/echo")

for i in range(len(subp_args)):
    # -I opotion: collect all instances of argument to be substituted for stdin
    if subp_args[i] == option["-I"]:
        _I_indices.append(i)
        

# process input
for feed in feeds:
    # determine if feed substitutes -I string
    if option["-I"]:
        for index in _I_indices:
            subp_args[index] = feed
    else:
        subp_args.append(feed)
        
    subprocess.run(subp_args)
    # subprocess.Popen(subp_args)
    
    if option["-I"]:
        continue
    else:
        subp_args.pop()
   