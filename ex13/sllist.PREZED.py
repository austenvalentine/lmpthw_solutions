class SingleLinkedListNode(object):
    
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt
        
    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        return f"[{self.value} : {repr(nval)}]"
        

class SingleLinkedList(object):
    
    def __init__(self):
        self.begin = None
        # this attribute name is 'end', but I use it as an index/pointer in all the methods
        self.end = None
        
    def push(self, obj):
        """Appends a new value on the end of the list."""
        push_node = SingleLinkedListNode(obj, None, None)
        
        if self.begin == None:
            self.begin = push_node
            self.end = push_node
        elif self.begin.nxt == None:
            self.begin.nxt = push_node
            self.end = push_node
        elif self.end.nxt == None:
            self.end.nxt = push_node
        else:
            self.end = self.end.nxt
            self.end.nxt = push_node
        
        
    def pop(self):
        """Removes the last item and returns it."""
        pop_val = None
        prev_node = None
        # reset the index
        self.end = self.begin
        
        if self.begin == None:
            return None
        elif self.begin.nxt == None:
            pop_val = self.end.value
            self.begin = None
            self.end = None
            return pop_val
            
        while self.end.nxt:
            # index through nxt objects until None
            prev_node = self.end
            self.end = self.end.nxt
        
        if prev_node:
            prev_node.nxt = None
        pop_val = self.end.value
        self.end = prev_node
        
        return pop_val
            
        
    def shift(self, obj):
        """Places object at beginning of list"""
        shift_node = SingleLinkedListNode(obj, self.begin, None)
        self.begin = shift_node
        
        
    def unshift(self):
        """removes the first item and returns it."""
        unshift_val = None
        
        if self.begin:
            unshift_val = self.begin.value
            self.begin = self.begin.nxt
        
        return unshift_val
        
        
    def remove(self, obj):
        """Finds a matching item and removes it from the list. Returns index."""
        self.end = self.begin
        prev_node = self.begin
        
        if not self.begin:
            return None
        elif self.begin.value == obj:
            # edge case if first element is a match
            self.begin = self.begin.nxt
            return 0
        
        index = 0
        while self.end:
            if self.end.value == obj:
                prev_node.nxt = self.end.nxt
                self.end = prev_node
                return index
            prev_node = self.end
            self.end = self.end.nxt
            index += 1
        
        return None
        
        
    def first(self):
        """Returns a reference to the first item, does not remove."""
        if self.begin:
            return self.begin.value
        else:
            return None
            
            
    def last(self):
        """Returns a reference to the last item, does not remove."""
        self.end = self.begin
        if self.end == None:
            return None
        else:
            while self.end.nxt:
                self.end = self.end.nxt
            return self.end.value
        
    def count(self):
        """Counts the number of elements in the list."""
        if self.begin == None:
            return 0
        else:
            self.end = self.begin
            count = 1
            while self.end.nxt:
                self.end = self.end.nxt
                count +=1
                
            return count
            
    def get(self, index):
        """Get the value at index."""
        self.end = self.begin
        counter = 0
        # move the end to the index. return None if there are no more nodes
        while counter < index:
            if self.end.nxt:
                self.end = self.end.nxt
            else:
                return None
            counter += 1
        
        if self.end:
            return self.end.value
        else:
            return None
            

    def dump(self, mark):
        """Debugging function that dumps the contents of the list."""
        








#### Test Code
animals = SingleLinkedList()
animals.push("sheep")
animals.push("horse")
animals.push("goat")
animals.push("cow")
animals.push("pig")
animals.push("dog")
animals.push("rabbit")

print("animals.remove('cow'): ", animals.remove("cow"))
print("animals.last(): ", animals.last(), animals.count())
print("animals.remove('goose'): ", animals.remove("goose"))
print("animals.last(): ", animals.last(), animals.count())
