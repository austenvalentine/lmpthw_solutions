class SingleLinkedListNode(object):
    
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt
        self.prev = prev
        
    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        return f"[{self.value}:{repr(nval)}]"
        
class Controller(object):
    """create push, pop, first and count methods"""
    def __init__(self):
        self.current = None
        self.start = None
        self.last = None
    
    def push(self, value):
        if self.current == None:
            new_node = SingleLinkedListNode(self, value, None, None)
        else:
            new_node = SingleLinkedListNode(self, value, None, self.current)
        self.current = new_node
        
    def first(self):
        if self.current.prev:
            self.first(self.current.prev)
        else:
            return self.current
            
            
            
con = Controller()

con.push(6)

print(">>>con.push(6): ", con.last)

con.push(7)

print(">>>con.push(7): ", con.last)

con.push(12)

print(">>>con.push(12): ", con.last)