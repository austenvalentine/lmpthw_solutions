class SingleLinkedListNode(object):
    
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt

    def __repr__(self):
        nval = self.nxt and self.nxt.value or None
        return f"[{self.value}:{repr(nval)}]"
        

class Controller(object):
    """create push, pop, first and count methods"""
    def __init__(self, start_node=None):
        self.start_node = start_node
        self.index_node = start_node
    
    
    def last(self):
        """move index to the last node"""
        if self.index_node.nxt:
            nxt_node = self.index_node.nxt
            self.index_node = nxt_node
            self.last()
            
        return self.index_node
            
            
    def push(self, value):
        """ append a new node to the last existing node """
        push_node = SingleLinkedListNode(value, None, None)
        if self.start_node == None:
            self.start_node = push_node
            self.index_node = push_node
        elif self.start_node.nxt == None:
            self.start_node.nxt = push_node
            self.index_node = self.start_node.nxt
        else:
            self.index_node = self.last()
            self.index_node.nxt = push_node
            
            
    def nxn(self):
        """advance index to next node"""
        if self.index_node.nxt:
            self.index_node = self.index_node.nxt
            return self.index_node
            
        self.index_node = None
        
        
    def __repr__(self):
        self.index_node = self.start_node
        output = ''
        while self.index_node:
            output += f"{self.index_node.value}"
            if self.index_node.nxt:
                output += ', '
            self.nxn()
            
        return '[' + output + ']'

    
            
            
con = Controller()

print(">>>con.push(15)")
con.push(15)

print("con.start_node", con.start_node)
print("con.start_node.nxt", con.start_node.nxt)
print("con.index_node", con.index_node)
print("con.index_node.nxt", con.index_node.nxt)

print(">>>con.push(22)")
con.push(22)

print("con.start_node", con.start_node)
print("con.start_node.nxt", con.start_node.nxt)
print("con.index_node", con.index_node)
print("con.index_node.nxt", con.index_node.nxt)

print(">>>con.push(36)")
con.push(36)

print("con.start_node", con.start_node)
print("con.start_node.nxt", con.start_node.nxt)
print("con.index_node", con.index_node)
print("con.index_node.nxt", con.index_node.nxt)

print(">>>con.push(47)")
con.push(47)

print("con.start_node", con.start_node)
print("con.start_node.nxt", con.start_node.nxt)
print("con.index_node", con.index_node)
print("con.index_node.nxt", con.index_node.nxt)

print(">>>con.push(51)")
con.push(51)

print("con.start_node", con.start_node)
print("con.start_node.nxt", con.start_node.nxt)
print("con.index_node", con.index_node)
print("con.index_node.nxt", con.index_node.nxt)

print(">>>con: ", con)