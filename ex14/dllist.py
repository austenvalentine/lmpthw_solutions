class DoubleLinkedListNode(object):
    
    def __init__(self, value, nxt, prev):
        self.value = value
        self.nxt = nxt
        self.prev = prev
        
    def __repr__(self):
        nval = self.next and self.next.value or None
        pval = self.prev and self.prev.value or None
        return f"[{self.value}, {repr(nval)}, {repr(pval)}]"
        

class DoubleLinkedList(object):
    
    def __init__(self):
        self.begin = None
        self.end = None
        

    def push(self, obj):
        """Appends a new value on the end of the list."""
        if self.begin == None:
            push_node = DoubleLinkedListNode(obj, None, None)
            self.begin = push_node
            self.end = self.begin
        else:
            push_node = DoubleLinkedListNode(obj, None, self.end)
            self.end.nxt = push_node
            self.end = self.end.nxt
    
    def pop(self):
        """Removes the last item and returns it."""
        pop_val = None
        if self.end == None:
            return pop_val
        elif self.end.prev == None:
            pop_val = self.end.value
            self.end = None
            self.begin = None
            return pop_val
        else:
            pop_val = self.end.value
            self.end = self.end.prev
            return pop_val
    
    def shift(self, obj):
        """Actually just another name for push."""
        if self.begin == None:
            shift_node = DoubleLinkedListNode(obj, None, None)
            self.begin = shift_node
            self.end = self.begin
        else:
            shift_node = DoubleLinkedListNode(obj, self.begin, None)
            self.begin.prev = shift_node
            self.begin = shift_node
        
    def unshift(self):
        """Removes the first item (from begin) and returns it."""
        unshift_val = None
        if self.begin:
            unshift_val = self.begin.value
            if self.begin.nxt:
                self.begin = self.begin.nxt
            else:
                self.begin = None
                self.end = self.begin
        return unshift_val
        
    def detach_node(self, obj):
        """You'll need to use this operation sometimes, but mostly
        inside remove(). It should take a node, and detach it from the list, whether the the node is at the front, end or in the middle."""
        assert self.begin != None
        assert self.end != None
        if self.begin == self.end:
            self.begin = None
            self.end = self.begin
        elif obj == self.begin:
            self.begin = self.begin.nxt
        elif obj == self.end:
            self.end = self.end.prev
        else:
            prev_node = obj.prev
            nxt_node = obj.nxt
            prev_node.nxt = nxt_node
            nxt_node.prev = prev_node
        
        
    def remove(self, obj):
        """Finds a matching item and removes it from the list."""
        itr = self.begin
        index = 0
        while itr:
            if itr.value == obj:
                self.detach_node(itr)
                return index
            itr = itr.nxt
            index += 1
        return None

    def first(self):
        """Returns a *reference* to the first item, does not remove."""
        if self.begin:
            return self.begin.value
        else:
            return None
    
    def last(self):
        """Returns a reference to the last item, does not remove."""
        if self.end:
            return self.end.value
        else:
            return None
            
    def count(self):
        """Count the number of elements in the list."""
        itr = None
        count = 0
        if self.begin == None:
            return count
        else:
            itr = self.begin
            count +=1
            while itr != self.end:
                itr = itr.nxt
                count += 1
            return count
        
        
    def get(self, index):
        """Get the value at index."""
        if index > self.count() - 1:
            return None
        get_node = self.begin
        count = 0
        while get_node:
            if count > index:
                return None
            elif count == index:
                return get_node.value
            else:
                get_node = get_node.nxt
                count += 1
        
        
        
    def dump(self, mark):
        """Debugging function that dumps the contents of the list."""
            
    def _invariant(self):
        if self.begin == None:
            assert self.end == None
