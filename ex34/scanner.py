import re

# This is my third attempt at the scanner-parser from ex32-33. This time, the NewLine class
# tracks empty lines, newlines and indent/dedent. No whitespace or indent tokens are passed
# to the parser.

class Token(object):
    def __init__(self, line_token, token_id, lexeme, start, end):
        self.line_token = line_token
        self.token_id = token_id
        self.lexeme = lexeme
        self.lexstart = start
        self.lexend = end
        
    def __repr__(self):
        return f"{self.token_id}: '{self.lexeme}',\t({self.lexstart}, {self.lexend})"
        
class NewLine(Token):
    """ Special Token to handle newlines and track indenting"""
    # maybe write a parser function to handle newline/tree tracking
    def __init__(self, line_number, line, previous_newline=None):
        self.token_id = 'NEWLINE'
        self.line_number = line_number
        self.empty_flag = True
        self.indent_level = 0
        self.prev_newline = previous_newline
        self.line = line
    
    def __repr__(self):
        return f"\n>>>NLTOKEN: LINENUMBER:{self.line_number}, INDENT:{self.indent_level}"
            
        
class Scanner(object):
    def __init__(self, token_list, code):
        # compile regex patterns from token list
        self.regexes = self.build_regexes(token_list)
        # convert source code string to list of lines
        self.code = self.clean_code(code)
        self.tokens, self.newline_tokens = self.scan()
    
    def build_regexes(self, token_list):
        """ build the token-pattern lookup list: [(t_id, regex),]
        ensure that the 4-space tab pattern is matched before
        the 1-space whitespace pattern to track indenting
        """
        regexes = []
        for pattern, token_id in token_list:
            regexes.append((token_id, re.compile(pattern)))
        return regexes
        
    def clean_code(self, code):
        """ convert source code string into split lines """
        cleaned = []
        for line in code.splitlines():
            # don't strip empty lines
            stripped = line.rstrip()
            if stripped:
                cleaned.append(stripped)
            else:
                cleaned.append(line)
        return cleaned
    
    def scan(self):
        """scan source code to match and build token stream
        dispose of non-tab whitespace"""
        ### REFACTOR THIS SUCKER
        assert self.code, "ERROR: empty source code"
        tokens = []
        newline_tokens = []
        newline_token = None
        for line_number in range(len(self.code)):
            line = self.code[line_number]
            # start current line with NewLine token
            newline_token = NewLine(line_number, line, newline_token)
            newline_tokens.append(newline_token)
            indent_tally = 0
            i = 0
            while i < len(line):
                token_result = self.match_token(line, i, newline_token)
                assert token_result, f"ERROR: no match for match_token('{line[i:]}')"
                if newline_token.empty_flag and token_result.token_id == 'INDENT':
                    # count leading indents
                    indent_tally += 1
                elif token_result.token_id != 'WHITESPACE' and token_result.token_id != 'INDENT':
                    # stop counting indents if non-whitespace token found
                    newline_token.empty_flag = False
                    tokens.append(token_result)
                i += len(token_result.lexeme)
            ## record indent levels
            if not newline_token.empty_flag:
                #Non-empty line gets indent tally
                newline_token.indent_level = indent_tally
            elif newline_token.prev_newline:
                # EMPTY line preserves indent level from previous line
                newline_token.line_number = newline_token.prev_newline.line_number
            else:
                # first token leaves indent at 0
                pass
        # maybe leave this newline check to the parser or analyzer
        if tokens and tokens[0].line_token.indent_level > 0:
            assert False, "Unexpected leading indent at line 0"
        return tokens, newline_tokens

    def match_token(self, line, i, line_token):
        """ Return Token object by taking index and line
        to match pattern from token regexes
        """
        for token_id, regex in self.regexes:
            match_result = regex.match(line[i:])
            if match_result:
                lexstart = i
                lexend = i + match_result.end()
                lexeme = line[i:lexend]
                return Token(line_token, token_id, lexeme, lexstart, lexend)
        return None
        
    def peek(self):
        if self.tokens:
            return self.tokens[0].token_id
    
    def peek_indent(self):
        if self.tokens:
            return self.tokens[0].line_token.indent_level
        else:
            return None
    
    def get_code(self):
        if self.tokens:
            return self.tokens[0].line_token.line
        else:
            assert False, "no tokens!"
            
    def match(self, token_id):
        while token_id == 'NEWLINE':
            self.tokens.pop(0)
        if token_id == self.tokens[0].token_id:
            return self.tokens.pop(0)
            
    def push(self, token):
        self.tokens.insert(0, token)