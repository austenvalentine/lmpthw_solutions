class Production(object):
    pass

class Root(Production):
    def __init__(self, body):
        self.prod_type = "ROOT"
        self.body = body
        
    def visit(self, world):
        for elt in self.body:
            elt.visit(world)
        # self.body.visit(world)
        return self.body
        
    def __repr__(self):
        return f"\n<ROOT>\n body: {self.body}\n</ROOT>"

class FuncDef(Production):
    def __init__(self, name_token, args_formal, body):
        self.prod_type = "FUNCDEF"
        if type(name_token) == str:
            self.name = name_token
        else:
            self.name = name_token.lexeme
        self.args_formal = args_formal
        self.body = body
        
    def visit(self, world):
        print(">>>FuncDefNAME: ", self.name)
        world.functions[self.name] = self
        self.args_formal.visit(world)
        self.body.visit(world)
    
    def __repr__(self):
        return f"\n<FUNCDEF>\n name: {self.name} \n args_formal: {self.args_formal} \n body: {self.body}\n</FUNCDEF>"
                
        
class Params(Production):
    def __init__(self, parameters):
        self.prod_type = "PARAMS"
        self.parameters = parameters
    
    def visit(self, world):
        for elt in self.parameters:
            elt.visit(world)
            
    def __repr__(self):
        return f"\n<PARAMS>\n parameters: {self.parameters}\n</PARAMS>"

        
class ArgsFormal(Production):
    def __init__(self, arguments):
        self.prod_type = "ARGSFORMAL"
        self.arguments = arguments
        
    def visit(self, world):
        for elt in self.arguments:
            elt.visit(world)
        
    def __repr__(self):
        return f"\n<ARGSFORMAL>\n arguments {self.arguments}\n</ARGSFORMAL>"
        
        
class Body(Production):
    def __init__(self, contents, indent_level=0):
        self.prod_type = "BODY"
        self.indent_level = indent_level
        self.contents = contents

    def visit(self, world):
        for elt in self.contents:
            elt.visit(world)
        
    def __repr__(self):
        return f"\n<BODY indent: {self.indent_level} >\n contents: {self.contents}\n</BODY>"

class ExprVariable(Production):
    def __init__(self, var_token):
        self.prod_type = "EXPRVAR"
        if type(var_token) == str:
            self.identifier = var_token
        else:
            self.identifier = var_token.lexeme

    def visit(self, world):
        pass
        
    def __repr__(self):
        return f"\n<EXPRVAR>\n identifier: {self.identifier}\n</EXPRVAR>"
        
class ArgF(Production):
    # a formal argument
    def __init__(self, var_token):
        self.prod_type = "ARGF"
        if type(var_token) == str:
            self.identifier = var_token
        else:
            self.identifier = var_token.lexeme

    def visit(self, world):
        world.variables[self.identifier] = None
        
    def __repr__(self):
        return f"\n<ARGF>\n identifier: {self.identifier}\n</ARGF>"

        
class ExprNumber(Production):
    def __init__(self, num_token):
        self.prod_type = "EXPRNUM"
        if type(num_token) == int:
            self.value = num_token
        else:
            self.value = int(num_token.lexeme)
        
    def visit(self, world):
        pass
        
    def __repr__(self):
        return f"\n<EXPRNUM>\n value: {self.value}\n</EXPRNUM>"
        
class ExprPlus(Production):
    def __init__(self, left, right):
        self.prod_type = "EXPRPLUS"
        self.left = left
        self.right = right
        
    def visit(self, world):
        self.left.visit(world)
        self.right.visit(world)
    
    def __repr__(self):
        return f"\n<EXPRPLUS>\n left: {self.left}\n right: {self.right}\n</EXPRPLUS>"
        
class ExprFCall(Production):
    def __init__(self, function_prod):
        self.prod_type = "EXPRFCALL"
        self.function_call = function_prod
        
    def visit(self, world):
        self.function_call.visit(world)
        
    def __repr__(self):
        return f"\n<EXPRFCALL>\n function_call: {self.function_call}\n</EXPRFCALL>"
        
class FuncCall(Production):
    def __init__(self, name_token, params):
        self.prod_type = "FUNCCALL"
        if type(name_token) == str:
            self.name = name_token
        else:
            self.name = name_token.lexeme
        self.params = params
        
    def visit(self, world):
        # lookup the function definition in the function dict
        # and grab its list of formal arguments
        formal_args = world.functions[self.name].args_formal.arguments
        # grab the functional call's list of actual arguments (parameters)
        
        # zip them into an iterator
        
        # loop through so each formal argument's name/identifier in the
        # world.variables dict is assigned the expression in the calling
        # functions actual arguments (paramters)
        
        
        # pass world up through the tree
        self.params.visit(world)
    
    def __repr__(self):
        return f"\n<FUNCCALL>\n name: {self.name}\n params: {self.params}\n</FUNCCALL>"
