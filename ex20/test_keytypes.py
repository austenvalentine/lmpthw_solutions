from bstree import *
from dictionary import *
import random, string

max_numbers = 800

def get_kv(max_nums=max_numbers):
    value_length = 16
    numbers = list(random.sample(range(0, max_nums), max_nums))
    words = list()
    for i in range(max_nums):
        word = ''.join(random.choice(string.ascii_lowercase) for x in range(value_length))
        words.append(word)
    kv_pairs = list(zip(numbers, words))
    return kv_pairs
    
kv_list = get_kv()

def test_dictionary(kv=kv_list[:]):
    kv = kv_list[:]
    d = Dictionary()
    for k,v in kv_list:
        d.set(k,v)
    for k,v in kv_list:
        d.get(k)
        
def test_bstree(kv=kv_list[:]):
    d = BSTree()
    for k,v in kv_list:
        d.set(k,v)
    for k,v in kv_list:
        d.get(k)

if __name__=="__main__":
    test_bstree()
    test_dictionary()