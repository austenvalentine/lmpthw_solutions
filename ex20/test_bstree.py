from bstree import *

def test_set():
    g = BSTree()
    g.set(123, 'first node')
    g.set(132, 'second node')
    g.set(231, 'third node')
    g.set(213, 'fourth node')
    g.set(312, 'fifth node')
    g.set(321, 'sixth node')
    g.set('chuck', 'first name')
    g.set('spence', 'last name')
    print("g.get(312): ", g.get(312))
    print("g.get(321): ", g.get(321))
    print("g.get(213): ", g.get(213))
    print("g.get(231): ", g.get(231))
    print("g.get(132): ", g.get(132))
    print("g.get(123): ", g.get(123))
    print("g.get('chuck': ", g.get('chuck'))
    print("g.get('spence': ", g.get('spence'))
    
def test_refer_node():
    a = BSTree()
    a.set('Botswana', 'TEST2')
    a.set('Zambia', None)
    a.set('Tunisia', None)
    a.set('Morocco', None)
    a.set('Democratic Republic of the Congo', None)
    a.set('South Africa', None)
    a.set('Egypt', None)
    a.set('Namibia', None)
    a.set('Ivory Coast', None)
    a.set('The Gambia', None)
    a.set('Central African Republic', None)
    a.set('Kenya', None)
    a.set('Ethiopia', None)
    a.set('Chad', None)
    a.set('Zimbabwe', None)
    a.set('Somalia', None)
    a.set('Mali', None)
    a.set('Algeria', None)
    a.set('Benin', None)
    a.set('Angola', None)
    a.set('Burkina Faso', None)
    a.set('Burundi', None)
    a.set('Cameroon', None)
    a.set('Cape Verdi', None)
    a.set('Nigeria', None)
    a.set('Comoros', None)
    a.set('Mauritius', None)
    a.set('Republic of the Congo', None)
    a.set('Djibouti', None)
    a.set('Eritrea', None)
    a.set('Equitorial Guinea', None)
    a.set('Gabon', None)
    a.set('Gambia', None)
    a.set('Ghana', 'TEST')
    assert a.refer_node('Ghana').value == 'TEST'
    assert a.refer_node('Botswana').value == 'TEST2'
    
    
def test_delete():
    g = BSTree()
    g.set('Michigan', 'MI')
    g.set('Florida', 'FL')
    g.set('Oregon', 'OR')
    g.set('Alaska', 'AK')
    g.set('Minnesota', 'MN')
    g.set('Wyoming', 'WY')
    g.set('Pennsylvania', 'PA')
    g.set('Hawaii', 'HI')
    g.set('Alabama', 'AL')
    g.set('Conneticut', 'CT')
    g.set('Kentucky', 'KY')
    g.list()
    print('delete Alabama, Conneticut')
    g.delete('Alabama')
    g.delete('Conneticut')
    g.list()
    print('set Alabama, Conneticut')
    g.set('Alabama', 'AL')
    g.set('Conneticut', 'CT')
    g.list()
    print('>>>delete Michigan')
    g.delete('Michigan')
    g.list()
    print('>>>set Michigan')
    g.set('Michigan', 'MI')
    g.list()
    print('>>>delete Hawaii')
    g.delete('Hawaii')
    g.list()
    print('>>>set Hawaii')
    g.set('Hawaii', 'HI')
    g.list()
    print('>>>delete Kentucky')
    g.delete('Kentucky')
    g.list()
    print('>>>delete Kentucky')
    g.delete('Kentucky')
    g.list()