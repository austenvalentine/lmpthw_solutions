import subprocess
import sys
import readline
import os

readline.parse_and_bind('tab: complete')

def get_user():
    command_string = ''
    while not command_string:
        command_string = input(">")
    return command_string
    
def parse_user(command_string):
    command_parsed = command_string.strip().split(' ')
    while command_parsed[0] != ' ':
        break
    return command_parsed

def command_run(parsed):
    if parsed == None:
        return
    elif parsed[0] == 'cd':
        try:
            os.chdir(parsed[1])
        except:
            print(f"{parsed[1]}: File or directory does not exist")
    elif parsed[0] == 'exit':
        exit(0)
    else:
        try:
            subprocess.run(parsed)
        except:
            print(''.join(parsed), " is an invalid command")
    return

if __name__=="__main__":
    while True:
        command_string = get_user()
        command_parsed = parse_user(command_string)
        feedback = command_run(command_parsed)