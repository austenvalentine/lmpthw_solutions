import re

class InvalidToken(Exception):
    def __init__(self, line_num, line, tokens):
        mesg = f"ERROR Invalid Token {line_num}: \"{line}\"" \
        + f"\nTOKENS:{tokens}"
        Exception.__init__(self, mesg)

class Token(object):
    def __init__(self, line_num, label, lexeme, start, end):
        self.line_num = line_num
        self.label = label
        self.lexeme = lexeme
        self.start = start
        self.end = end

    def __repr__(self):
        return f"<line: {self.line_num} ({self.start},{self.end}) {self.label}: " \
                + f"\"{self.lexeme}\">"
                

class Scanner(object):
    def __init__(self, patterns, mathin):
        self.mathin = mathin
        regexps = []
        for (pattern, label) in patterns:
            regexps.append((re.compile(pattern), label))
        self.tokens = []
        self.tokenize(regexps, mathin)

    def tokenize(self, regexps, mathin):
        for line_num, line in enumerate(mathin.split('\n')):
            self.match_math(line_num + 1, line, regexps)

    def match_math(self, line_num, line, regexps):

        match_index = 0
        line_length = len(line)
        while match_index <= line_length:
            if len(line[match_index:]) == 0:
                break
            token = None
            for regexp, label in regexps:
                result = regexp.match(line[match_index:])
                if result:
                    start = match_index
                    match_index = start + result.end()
                    lex = result.group().rstrip()
                    end = start + len(lex)
                    token = Token(line_num, label, lex, start, end)
                    self.tokens.append(token)
                    break 
            if token == None and len(line[match_index:]) > 0:
                raise InvalidToken(line_num, line[match_index:], self.tokens)
