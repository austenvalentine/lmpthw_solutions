#!/usr/bin/env python3
import re
import parser 

PATTERNS = (
                ("^if\s*", "IF"),
                ("^else\s*", "ELSE"),
                ("^while\s*", "WHILE"),
                ("^for\s*", "FOR"),
                ("^\;\s*", "SEMICOLON"),
                ("^[A-Za-z]+\s*", "VARIABLE"),
                ("^(\=\=)\s*", "EQTO"),
                ("^(\!\=)\s*", "NOTEQTO"),
                ("^(\>\=)\s*", "GREQTO"),
                ("^(\<\=)\s*", "LSEQTO"),
                ("^(\>)\s*", "GRTHAN"),
                ("^(\<)\s*", "LSTHAN"),
                ("^(\%\=)\s*", "MODASGN"),
                ("^(\/\=)\s*", "QUOASGN"),
                ("^(\*\=)\s*", "PROASGN"),
                ("^(\+\=)\s*", "SUMASGN"),
                ("^(\-\=)\s*", "DIFASGN"), 
                ("^\=\s*", "ASSIGN"),
                ("^[0-9]+\.?[0-9]*\s*", "NUMBER"),
                ("^\.[0-9]+\s*", "NUMBER"),
                ("^\(\s*", "LPAREN"),
                ("^\)\s*", "RPAREN"),
                ("\^\s*", "POW"),
                ("\%\s*", "MOD"),
                ("\/\s*", "QUO"),
                ("\*\s*", "PRO"),
                ("\+\s*", "SUM"),
                ("^\-\s*", "NEG")
)

MATH = """y=6
2 + x = 4 == y

y == 2 + x = 4
"""

def main():
    parsley = parser.Parser(PATTERNS, MATH) 
    for token in parsley.tokens: print(token)
    for statement in parsley.ast: print(statement)

if __name__=="__main__":
    main()
