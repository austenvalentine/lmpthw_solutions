import analyzer

class Interpreter(object):
    def __init__(self, patterns, math):
        self.mathin = math.split('\n')
        self.analyzer = analyzer.Analyzer(patterns,math)
        self.ast = self.analyzer.ast
        self.scope = analyzer.Scope()
        for elt in self.ast:
            print("TEST INPUT>>>", self.mathin[elt.line_num - 1])
            elt.run(self.scope)
