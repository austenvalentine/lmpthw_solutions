#!/usr/bin/env python3
import re
import interpreter

PATTERNS = (
                ("^if\s*", "IF"),
                ("^else\s*", "ELSE"),
                ("^while\s*", "WHILE"),
                ("^for\s*", "FOR"),
                ("^\;\s*", "SEMICOLON"),
                ("^[A-Za-z]+\s*", "VARIABLE"),
                ("^(\=\=)\s*", "EQTO"),
                ("^(\!\=)\s*", "NOTEQTO"),
                ("^(\>\=)\s*", "GREQTO"),
                ("^(\<\=)\s*", "LSEQTO"),
                ("^(\>)\s*", "GRTHAN"),
                ("^(\<)\s*", "LSTHAN"),
                ("^(\%\=)\s*", "MODASGN"),
                ("^(\/\=)\s*", "QUOASGN"),
                ("^(\*\=)\s*", "PROASGN"),
                ("^(\+\=)\s*", "SUMASGN"),
                ("^(\-\=)\s*", "DIFASGN"), 
                ("^\=\s*", "ASSIGN"),
                ("^[0-9]+\.?[0-9]*\s*", "NUMBER"),
                ("^\.[0-9]+\s*", "NUMBER"),
                ("^\(\s*", "LPAREN"),
                ("^\)\s*", "RPAREN"),
                ("\^\s*", "POW"),
                ("\%\s*", "MOD"),
                ("\/\s*", "QUO"),
                ("\*\s*", "PRO"),
                ("\+\s*", "SUM"),
                ("^\-\s*", "NEG")
)

MATH = """
x = -28
for (x = 10; x; x -= 1) y += 5
x
y
"""

def main():
    terp = interpreter.Interpreter(PATTERNS, MATH)
    print("!!! SYMBOL TABLE", terp.scope)

if __name__=="__main__":
    main()
