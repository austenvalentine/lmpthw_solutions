class AnalyzerInvalidSymbol(Exception):
    def __init__(self, name, scope):
        mesg = f"{name} is undeclared variable in {scope}"
        Exception.__init__(mesg)

class StatementList(object):
    def __init__(self, statements, line_num):
        self.statements = statements
        self.line_num = line_num

    def visit(self, scope):
        for elt in self.statements:
            elt.visit(scope)

    def run(self, scope):
        for elt in self.statements:
            result = elt.run(scope)
            if result != None: print(result)

    def __repr__(self):
        return f"\n<StatementList>{self.statements}\n</StatementList>"

class Statement(object):
    def __init__(self, content):
        self.content = content 

    def visit(self, scope):
        self.content.visit(scope)

    def run(self, scope):
        return self.content.run(scope)

    def __repr__(self):
        return f"\n<Statement>{self.content}\n</Statement>"

class Expression(object):
    def __init__(self, content):
        self.content = content

    def __repr__(self):
        return f"\n<Expression>{self.content}\n</Expression>"

    def visit(self, scope):
        self.content.visit(scope)

    def run(self, scope):
        return self.content.run(scope)

class IfStatement(object):
    def __init__(self, test, action_true, action_false):
        self.test = test
        self.action_true = action_true
        self.action_false = action_false

    def __repr__(self):
        return f"\n<IfStatement>\ntest:{self.test}\naction_true:{self.action_true} \
                \naction_false:{self.action_false}\n</IfStatement>"
    def visit(self, scope):
        self.test.visit(scope)
        self.action_true.visit(scope)
        self.action_false.visit(scope)

    def run(self, scope):
        if self.test.run(scope) == 0:
            self.action_false.run(scope)
        else:
            self.action_true.run(scope)

class WhileStatement(object):
    def __init__(self, test, action):
        self.test = test
        self.action = action

    def __repr__(self):
        return f"\n<WhileStatement>\ntest:{self.test}\naction:{self.action}\n\
                </WhileStatement>"

    def visit(self, scope):
        self.test.visit(scope)
        self.action.visit(scope)

    def run(self,scope):
        while 0 != self.test.run(scope):
            self.action.run()

class ForStatement(object):
    def __init__(self, start_expr, test, itr_expr, action):
        self.start_expr = start_expr
        self.test = test
        self.itr_expr = itr_expr
        self.action = action

    def __repr__(self):
        return f"\n<ForStatement>\nstart_expr:{self.start_expr}\ntest:{self.test}\
                \nitr_expr:{self.itr_expr}\naction:{self.action}\n</ForStatement>"
    def visit(self, scope):
        self.start_expr.visit(scope)
        self.test.visit(scope)
        self.itr_expr.visit(scope)
        self.action.visit(scope)

    def run(self, scope):
        self.start_expr.run(scope)
        while 0 != self.test.run(scope):
            self.action.run(scope)
            self.itr_expr.run(scope)

class Number(object):
    def __init__(self, value):
        # parameter value should be lexeme string
        # and converted to datatype on assignment
        self.value = int(value)

    def __repr__(self):
        return f"\n<Number>value:{self.value}</Number>"

    def visit(self, scope):
        pass

    def run(self, scope):
        return float(self.value)

class Parentheses(object):
    def __init__(self, expression):
        self.expression = expression

    def __repr__(self):
        return f"\n<Parentheses>{self.expression}\n</Parentheses>"

    def visit(self, scope):
        self.expression.visit(scope)

    def run(self, scope):
        return self.expression.run(scope)

class Variable(object):
    def __init__(self, identifier):
        self.id = identifier

    def __repr__(self):
        return f"\n<Variable>id:{self.id}</Variable>"

    def visit(self, scope):
        num = Number("0")
        try:
            assert scope.symbol_table[self.id]
        except:
            scope.symbol_table[self.id] = num

    def run(self, scope):
        return scope.symbol_table.get(self.id, 0)

class Negation(object):
    def __init__(self, expression):
        # at the interpreter stage, evaluate (-1 * expression)
        self.expression = expression

    def __repr__(self):
        return f"\n<Negation>{self.expression}\n</Negation>"

    def visit(self, scope):
        self.expression.visit(scope)

    def run(self, scope):
        return -1 * self.expression.run(scope)

class BinaryOperator(object):
    def __init__(self, left_expr, right_expr):
        self.left_expr = left_expr
        self.right_expr = right_expr

    def __repr__(self):
        return f"\n<BinaryOperator>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</BinaryOperator>"

    def visit(self, scope):
        self.left_expr.visit(scope)
        self.right_expr.visit(scope)

class Assign(BinaryOperator):
    def __repr__(self):
        return f"\n<Assign>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Assign>"

    def visit(self,scope):
        assert type(self.left_expr) == Variable, SyntaxError()
        scope.symbol_table[self.left_expr.id] = self.right_expr
        self.right_expr.visit(scope)

    def run(self, scope):
        result = self.right_expr.run(scope)
        scope.symbol_table[self.left_expr.id] = result
        return result
        

class PowAsgn(Assign): 
    def __repr__(self):
        return f"\n<PowAsgn>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</PowAsgn>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS ^ RHS
        scope.symbol_table[self.left_expr.id] = result
        return result

class ModAsgn(Assign): 
    def __repr__(self):
        return f"\n<ModAsgn>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</ModAsgn>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS % RHS
        scope.symbol_table[self.left_expr.id] = result
        return result
    
class QuoAsgn(Assign): 
    def __repr__(self):
        return f"\n<QuoAsgn>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</QuoAsgn>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS / RHS
        scope.symbol_table[self.left_expr.id] = result
        return result

class ProAsgn(Assign): 
    def __repr__(self):
        return f"\n<ProAsgn>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</ProAsgn>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS * RHS
        scope.symbol_table[self.left_expr.id] = result
        return result

class SumAsgn(Assign): 
    def __repr__(self):
        return f"\n<SumAsgn>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</SumAsgn>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS + RHS
        scope.symbol_table[self.left_expr.id] = result
        return result
    
class DifAsgn(Assign):
    def __init__(self, left_expr, right_expr):
        self.left_expr = left_expr
        self.right_expr = right_expr

    def __repr__(self):
        return f"\n<DifAsgn>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</DifAsgn>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS - RHS
        scope.symbol_table[self.left_expr.id] = result
        return result

class Pow(BinaryOperator):
    def __repr__(self):
        return f"\n<Pow>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Pow>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS ^ int(RHS)
        return result


class Mod(BinaryOperator):
    def __repr__(self):
        return f"\n<Mod>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Mod>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS % RHS 
        return result

class Quo(BinaryOperator):
    def __repr__(self):
        return f"\n<Quo>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Quo>"
    
    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS / RHS 
        return result

class Pro(BinaryOperator):
    def __repr__(self):
        return f"\n<Pro>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Pro>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS * RHS 
        return result

class Sum(BinaryOperator):
    def __repr__(self):
        return f"\n<Sum>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Sum>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS + RHS 
        return result
    
class Dif(BinaryOperator):
    def __repr__(self):
        return f"\n<Dif>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Dif>"

    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.right_expr.run(scope)
        result = LHS - RHS 
        return result

class Relational(BinaryOperator):
    def __repr__(self):
        return f"\n<Relational>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</Relational>"

class EqTo(Relational):
    def __repr__(self):
        return f"\n<EqTo>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</EqTo>"
    
    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.left_expr.run(scope)
        if LHS == RHS:
            return 1
        else:
            return 0

class GrThan(Relational):
    def __repr__(self):
        return f"\n<GrThan>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</GrThan>"
    
    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.left_expr.run(scope)
        if LHS > RHS:
            return 1
        else:
            return 0

class LsThan(Relational):
    def __repr__(self):
        return f"\n<LsThan>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</LsThan>"
    
    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.left_expr.run(scope)
        if LHS < RHS:
            return 1
        else:
            return 0

class GrEqTo(Relational):
    def __repr__(self):
        return f"\n<GrEqTo>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</GrEqTo>"
    
    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.left_expr.run(scope)
        if LHS >= RHS:
            return 1
        else:
            return 0

class LsEqTo(Relational):
    def __repr__(self):
        return f"\n<LsEqTo>\nleft_expr:{self.left_expr}\nright_expr:{self.right_expr}\n</LsEqTo>"
    
    def run(self, scope):
        LHS = self.left_expr.run(scope)
        RHS = self.left_expr.run(scope)
        if LHS <= RHS:
            return 1
        else:
            return 0

