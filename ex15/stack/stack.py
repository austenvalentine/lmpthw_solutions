class StackNode(object):
    
        def __init__(self, value, nxt):
            self.value = value
            self.nxt = nxt
            
        def __repr__(self):
            nval = self.nxt and self.nxt.value or None
            return f"[{self.value}:{repr(nval)}]"
            
            
class Stack(object):
    
    def __init__(self):
        self.top = None
        
    def push(self, obj):
        """Pushes a new value to the top of stack."""
        if self.top == None:
            push_node = StackNode(obj, None)
            self.top = push_node
        else:
            push_node = StackNode(obj, self.top)
            self.top = push_node
            
    def pop(self):
        """Pops the value that is currently on the top of the stack."""
        pop_val = self.top and self.top.value or None
        self.top = self.top and self.top.nxt or None
        return pop_val
        
    def tippy_top(self):
        """returns a reference to the top item, does not remove."""
        top_val = self.top and self.top.value or None
        return top_val
        
    def count(self):
        """Counts the number of elements in the stack."""
        index = self.top
        count = 0
        while index:
            count += 1
            index = index.nxt
        return count
        
    def dump(self, mark="----"):
        """Debugging function that dumps the contents of the stack."""
        
    def _invariant(self):
        """Not worth checking for invariant conditions since there's only one
        attribute"""