from stack import *

def test_push():
    sink = Stack()
    assert sink.count() == 0
    sink.push("plate")
    assert sink.count() == 1
    sink.push("saucer")
    assert sink.count() == 2
    sink.push("tray")
    assert sink.count() == 3
    
def test_pop():
    sink = Stack()
    assert sink.count() == 0
    sink.push("plate")
    assert sink.count() == 1
    sink.push("saucer")
    assert sink.count() == 2
    sink.push("tray")
    assert sink.count() == 3
    
    assert sink.pop() == "tray"
    assert sink.count() == 2
    assert sink.pop() == "saucer"
    assert sink.count() == 1
    assert sink.pop() == "plate"
    assert sink.count() == 0
    assert sink.pop() == None
    assert sink.count() == 0
    
def test_tippy_top():
    sink = Stack()
    assert sink.count() == 0
    sink.push("plate")
    assert sink.count() == 1
    sink.push("saucer")
    assert sink.count() == 2
    sink.push("tray")
    assert sink.count() == 3
    
    assert sink.tippy_top() == "tray"
    assert sink.pop() == "tray"
    assert sink.count() == 2
    assert sink.tippy_top() == "saucer"
    assert sink.pop() == "saucer"
    assert sink.count() == 1
    assert sink.tippy_top() == "plate"
    assert sink.pop() == "plate"
    assert sink.count() == 0
    assert sink.tippy_top() == None
    assert sink.pop() == None
    assert sink.count() == 0