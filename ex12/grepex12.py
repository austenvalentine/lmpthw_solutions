#!/usr/bin/env python3

import sys, argparse, glob, os, re

# parse command options
print(">>> sys.argv: ", sys.argv)

if '-f' in sys.argv:
    parser = argparse.ArgumentParser()
    # recursive
    parser.add_argument('-r', action='store_true')
    # line numbers
    parser.add_argument('-n', action='store_true')
    # search patterns from file
    parser.add_argument('-f', type=str, action='store')
    parser.add_argument('FILES', type=str, nargs='*')

else:
    parser = argparse.ArgumentParser()
    parser.add_argument('-r', action='store_true')
    parser.add_argument('-n', action='store_true')
    # I have to trick the parser into making a False value for '-f'
    parser.add_argument('-f', action='store_true')
    parser.add_argument('PATTERN', type=str, action='store')
    parser.add_argument('FILES', type=str, nargs='*')

args = parser.parse_args()

print(">>> args: ", args)


"""NAME
       grep, egrep, fgrep, rgrep - print lines matching a pattern

SYNOPSIS
       grep [OPTIONS] PATTERN [FILE...]
       grep [OPTIONS] -e PATTERN ... [FILE...]
       grep [OPTIONS] -f FILE ... [FILE...]

DESCRIPTION
       grep  searches  for  PATTERN in each FILE.  A FILE of "-" stands for standard input.  If no FILE is given, recursive searches
       examine the working directory, and nonrecursive searches read standard input.  By default, grep prints the matching lines.

       In addition, the variant programs egrep, fgrep and rgrep are the same as grep -E, grep -F, and grep -r, respectively.   These
       variants are deprecated, but are provided for backward compatibility."""




def get_files():
    global args
    g = []
    h = []
    for file in args.FILES:
        if os.path.isfile(file):
            g.extend(glob.glob(file))
        elif os.path.isdir(file):
            # recurse through directories
            g.extend(glob.glob(file + '/**', recursive=args.r))
        elif args.f == '-':
            g.append(input())
        else:
            print(file, " is not a file/directory!")
            exit(0)
            
    h = [elt for elt in g if os.path.isfile(elt)]
    
    return h


def get_patterns():
    patterns = []
    if os.path.isfile(args.f):
        with open(args.f) as fin:
            for line in fin.readlines():
                patterns.append(line)
    elif args.PATTERN:
        patterns.append(args.PATTERN)
    else:
        print("gimme a pattern")
        exit(0)
    
    return patterns
        
def search_results():
    for pat in get_patterns():
        repat = re.compile(pat)
        results = []
        for file in get_files():
            with open(file) as fin:
                for (num, line) in enumerate(fin.readlines()):
                    if re.search(repat, line):
                        results.append((file, num +1, line))
        
        return results
        
if args.n:
    for file, num, text in search_results():
        print(f"{file}: {num}: {text}", end='')
else:
    for file, num, text in search_results():
        print(f"{file}: {text}", end='')