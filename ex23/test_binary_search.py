from dllist import *
from bstree import *
import random, string
from bsearch import bsearch

max_numbers = 800

def get_words(max_nums=max_numbers):
    value_length = 16
    words = list()
    for i in range(max_nums):
        word = ''.join(random.choice(string.ascii_lowercase) for x in range(value_length))
        words.append(word)
    return words
    
words = get_words()
words_sorted = sorted(words)
search_words = random.sample(words, len(words))

def test_dllist(words=words_sorted, search_words=search_words):
    d = DoubleLinkedList()
    for word in words:
        d.push(word)
    for sword in search_words:
        d.bsearch(sword)
        
def test_dllist_insort(words=words, search_words=search_words):
    d = DoubleLinkedList()
    for word in words:
        d.insort(word)
    for sword in search_words:
        d.bsearch(sword)
        
def test_pylist(words=words_sorted, search_words=search_words):
    p = list()
    for word in words:
        p.append(word)
    for sword in search_words:
        bsearch(p, sword)
        
def test_BSTree(words=words, search_words=search_words):
    b = BSTree()
    for word in words:
        b.set(word, None)
    for sword in search_words:
        b.get(sword)
        
def test_BSTree_sorted(words=words_sorted, search_words=search_words):
    b = BSTree()
    for word in words:
        b.set(word, None)
    for sword in search_words:
        b.get(sword)

if __name__=="__main__":
    test_pylist()
    test_dllist()
    test_dllist_insort()
    test_BSTree()
    test_BSTree_sorted()
    