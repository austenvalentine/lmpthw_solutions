from suffix import *

num_iterations = 10000

def test_find_shortest():
    for i in range(num_iterations):
        sfx = SuffixArray("abracadabra")
        assert sfx.find_shortest("abra") == "abra"
    
def test_find_longest():
    for i in range(num_iterations):
        sfx = SuffixArray("abracadabra")
        assert sfx.find_longest("abra") == "abracadabra"
    
def test_find_all():
    for i in range(num_iterations):
        sfx = SuffixArray("abracadabra")
        assert sfx.find_all("abra") == ['abra', 'abracadabra', 'acadabra', 'adabra', 'bracadabra', 'cadabra', 'dabra', 'racadabra']

def test_find_allBST():
    for i in range(num_iterations):
        sfx = SuffixArray("abracadabra")
        # assert sfx.find_all("abra") == ['abra', 'abracadabra', 'acadabra', 'adabra', 'bracadabra', 'cadabra', 'dabra', 'racadabra']
        assert len(sfx.find_all("abra")) == 8
    
def test_find_shortestBST():
    for i in range(num_iterations):
        sfx = SuffixArray("abracadabra")
        assert sfx.find_shortest("abra") == "abra"
    
def test_find_longestBST():
    for i in range(num_iterations):
        sfx = SuffixArray("abracadabra")
        assert sfx.find_longest("abra") == "abracadabra"

    
if __name__=="__main__":
    test_find_shortest()
    test_find_longest()
    test_find_all()
    test_find_shortestBST()
    test_find_longestBST()
    test_find_allBST()