class SuffixArray(object):
    def __init__(self, whole_word):
        self.ww = whole_word
        self.sa = []
        for i in range(0, len(self.ww)):
            self.sa.append(self.ww[i:])
        self.sa = sorted(self.sa)
        
    def find_all(self, suffix):
        results = []
        length = len(suffix)
        for suf in self.sa:
            if suf[- length:] == suffix:
                results.append(suf)
        return results
    
    def find_shortest(self, suffix):
        return min(self.find_all(suffix), key=len)
        
    def find_longest(self, suffix):
        return max(self.find_all(suffix), key=len)
        

class SuffixBST(object):
    def __init__(self, whole_word):
        self.sa = BinarySearchTree()
        self.ww = whole_word
        for i in range(0, len(self.ww)):
            self.sa.set(self.ww[i:], i)
            
    def find_all(self, suffix):
        results = []
        for i in range(len(self.ww) - len(suffix)):
            suf = self.ww[- i - len(suffix):]
            if self.get(suf) != None:
                results.append(suf)
        # return sorted(results)
        return results
            
    def find_shortest(self, suffix):
        return min(self.find_all(suffix), key=len)
    
    def find_longest(self, suffix):
        return max(self.find_all(suffix), key=len)