from bstree import BSTree, BSTreeNode
from tstree import TSTree, TSTreeNode
from dllist import DoubleLinkedList, DoubleLinkedListNode
from dictionary import Dictionary
from bsearch import bsearch
from urlrouter import *
import random, string
##################################################
#Test Data
##################################################
# Routers:
# TSTRouter
# BSTRouter
# DLLRouter
# DictRouter
# PyDictRouter
which_router = TSTRouter
max_urls = 800
max_url_depth = 16
max_tier_length = 2

longer_url_depth = 32
longer_tier_length = 4

def get_urls(num_urls=max_urls, depth=max_url_depth, tier_length=max_tier_length):
    result_urls = []
    for i in range(num_urls):
        url = "/"
        for j in range(depth):
            tier = ""
            for k in range(tier_length):
                tier += random.choice(string.ascii_lowercase)
            url += tier + "/"
        result_urls.append(url)
    return(result_urls)

rand_urls = get_urls()
urlgets = sorted(rand_urls)
ru = rand_urls
ug = urlgets
lru = get_urls(max_urls, longer_url_depth, longer_tier_length)
lug = sorted(lru)

##################################################
#Test Functions
##################################################
router_subclass = None
    
def test_URLRouter_add(router=router_subclass):
    pass
    
def test_URLRouter_find_exact(router=router_subclass):
    pass

def test_URLRouter_find_all(router=router_subclass):
    pass

def test_URLRouter_find_shortest(router=router_subclass):
    pass

def test_URLRouter_find_best(router=router_subclass):
    pass

def test_URLRouter_find_longest(router=router_subclass):
    pass

def test_URLRouter_randomurls(random_urls=ru[:], url_gets=ug[:], which_router=which_router):
    """Performance"""
    urlr = which_router()
    for i, u in enumerate(random_urls):
        urlr.add(u, f"url{i}")
    for i in url_gets:
        urlr.find_exact(i)

def test_URLRouter_partialpaths(random_urls=ru[:], url_gets=ug[:], which_router=which_router):
    """Performance"""
    urlr = which_router()
    for i, u in enumerate(random_urls):
        urlr.add(u, f"url{i}")
    for i in url_gets:
        urlr.find_all(i[:random.randint(0, len(i))])

def test_URLRouter_nonpaths(random_urls=ru[:], url_gets=ug[:], which_router=which_router):
    """Performance"""
    rev_urls = [url[::-1] for url in url_gets]
    urlr = which_router()
    for i, u in enumerate(random_urls):
        urlr.add(u, f"url{i}")
    for i in rev_urls:
        urlr.find_all(i)

def test_URLRouter_longpaths(random_urls=lru[:], url_gets=lug[:], which_router=which_router):
    """Performance"""
    urlr = which_router()
    for i, u in enumerate(random_urls):
        urlr.add(u, f"url{i}")
    for i in url_gets:
        urlr.find_exact(i)
        
def TSTTest():
    print("TSTTest")
    test_router = TSTRouter
    test_URLRouter_randomurls(which_router=test_router)
    test_URLRouter_partialpaths(which_router=test_router)
    test_URLRouter_nonpaths(which_router=test_router)
    test_URLRouter_longpaths(which_router=test_router)
    
def DLLTest():
    print("DLLTest")
    test_router = DLLRouter
    test_URLRouter_randomurls(which_router=test_router)
    test_URLRouter_partialpaths(which_router=test_router)
    test_URLRouter_nonpaths(which_router=test_router)
    test_URLRouter_longpaths(which_router=test_router)

def BSTTest():
    print("BSTTest")
    test_router = BSTRouter
    test_URLRouter_randomurls(which_router=test_router)
    test_URLRouter_partialpaths(which_router=test_router)
    test_URLRouter_nonpaths(which_router=test_router)
    test_URLRouter_longpaths(which_router=test_router)

def DictTest():
    print("DictTest")
    test_router = DictRouter
    test_URLRouter_randomurls(which_router=test_router)
    test_URLRouter_partialpaths(which_router=test_router)
    test_URLRouter_nonpaths(which_router=test_router)
    test_URLRouter_longpaths(which_router=test_router)

def PyDictTest():
    print("PyDictTest")
    test_router = PyDictRouter
    test_URLRouter_randomurls(which_router=test_router)
    test_URLRouter_partialpaths(which_router=test_router)
    test_URLRouter_nonpaths(which_router=test_router)
    test_URLRouter_longpaths(which_router=test_router)

##################################################
#Performance Test
#>python -m cProfile -s cumtime test_urlrouter.py
##################################################

if __name__=="__main__":
    rand_urls = get_urls()
    urlgets = sorted(rand_urls)
    ru = rand_urls
    ug = urlgets
    lru = get_urls(max_urls, longer_url_depth, longer_tier_length)
    lug = sorted(lru)
    PyDictTest()
    TSTTest()
    BSTTest()
    DLLTest()
    DictTest()
    