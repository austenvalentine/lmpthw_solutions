class TSTreeNode(object):
    
    def __init__(self, key, value, low, eq, high):
        self.key = key
        self.low = low
        self.eq = eq
        self.high = high
        self.value = value
        
    def __repr__(self):
        return f"\nNODE<{self.key.upper()}>[{self.key}:{self.value}]; {self.key}_LO:{self.low}; {self.key}_EQ:{self.eq}; {self.key}_HI:{self.high}</ {self.key.upper()}>"
        
class TSTree(object):
    
    def __init__(self):
        self.root = None
        
    def _get(self, node, keys):
        key = keys[0]
        if key < node.key:
            # print(f"getlo {node.value}: {key} < {node.key}")
            return self._get(node.low, keys)
        elif key == node.key:
            # print(f"geteq {node.value}: {key} == {node.key}")
            if len(keys) > 1:
                return self._get(node.eq, keys[1:])
            else:
                return node.value
        else:
            # print(f"gethi {node.value}: {key} > {node.key}")
            return self._get(node.high, keys)
            
    def get(self, key):
        keys = [x for x in key]
        return self._get(self.root, keys)
        
    def _set(self, node, keys, value):
        next_key = keys[0]
        if not node:
            # what happens if you add the value here?
            node = TSTreeNode(next_key, None, None, None, None)
        if next_key < node.key:
            node.low = self._set(node.low, keys, value)
            # print(f"{value}_setlo {keys}: {next_key} < {node.key}")
        elif next_key == node.key:
            # print(f"{value}_seteq {keys}: {next_key} == {node.key}")
            if len(keys) > 1:
                node.eq = self._set(node.eq, keys[1:], value)
            else:
                # what happens if you DO NOT add the value here?
                node.value = value
        else:
            node.high = self._set(node.high, keys, value)
            # print(f"{value}_sethi {keys}: {next_key} > {node.key}")
        
        return node
        
    def set(self, key, value):
        keys = [x for x in key]
        self.root = self._set(self.root, keys, value)
        # print(self.root)
        
    def find_all(self, piece):
        keys = list(piece)
        results = []
        self._find_all(keys, results, self.root)
        kp_results = [(k) for k in results]
        results = []
        return kp_results
        
        
    def _find_all(self, keys, results, node, piece=''):
        """ the piece (a search string) attribute rebuilds the key through the course of node traversal.
        This applies to the initial search string and all extensions following the search string."""
        key = keys and keys[0] or None
        if node:
            if key == '.':
                # rewrite the original search string if there's a wildcard
                # print("wildcard: ", piece)
                key = node.key
                self._find_all(keys[:], results, node.low, piece)
                self._find_all(keys[:], results, node.high, piece)
            if key == None:
            # case: if search string was found, find extensions and return with values
                # print("key == None: piece:", piece + node.key)
                if node.value:
                    results.append((piece + node.key, node.value))
                self._find_all(None, results, node.eq, piece + node.key)
                self._find_all(None, results, node.low, piece)
                self._find_all(None, results, node.high, piece)
            elif key == node.key:
                keys.pop(0)
                piece = piece + node.key
                if not keys:
                    if node.value:
                        results.append((piece, node.value))
                    self._find_all(None, results, node.eq, piece)
                else:
                    
                    self._find_all(keys, results, node.eq, piece)
            elif key < node.key:
                self._find_all(keys, results, node.low, piece)
            else:
                self._find_all(keys, results, node.high, piece)
        return
                
    def find_shortest(self, piece):
        shortest = min([x[0] for x in self.find_all(piece)], key=len)
        return shortest, self.get(shortest)
        
    def find_longest(self, piece):
        longest =  max([x[0] for x in self.find_all(piece)], key=len)
        return longest, self.get(longest)
    
    def find_part(self, search_string):
        """Zed Sez: Given a key K, find the shortest key that has at least a part of the beginning of K. """
        first_key = list(search_string[0])
        results = []
        self._find_all(first_key, results, self.root)
        shortest = min([x[0] for x in results], key=len)
        shortest_value = self.get(shortest)
        return shortest, shortest_value
        
        
if __name__=='__main__':
    pass
