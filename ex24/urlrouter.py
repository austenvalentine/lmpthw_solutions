from tstree import *
from dllist import *
from bstree import *
from dictionary import *
from operator import attrgetter

class URLRouterNode(object):
    def __init__(self, url, action):
        self.url = url
        self.action = action

class URLRouter(object):
    
    def __init__(self):
        """First, design implementions for TSTree, DoubleLinkedList, BSTree, Dictionary and Python dict. Then, see which functions are common to all and move to this superclass"""
        self.urls = None
        
    def add(self):
        pass

    def find_exact(self):
        pass

    def find_best(self, qurl):
        """score+rank each result by character matches. choose the shortest len() result from top rank"""
        results = self.find_all(qurl[:2])
        if not results:
            return None, None
        # rank matches
        scores = []
        for result in results:
            node = result[1]
            longer = (len(node.url) > len(qurl)) and node.url or qurl
            shorter = (longer == node.url) and qurl or node.url
            node.score = 0
            for i in range(len(shorter)):
                if shorter[i] == qurl[i]:
                    node.score += 1
                else:
                    break
            scores.append(node)
        scores.sort(key=lambda x: len(x.url))
        scores.sort(key=lambda x: x.score)
        best = scores.pop(0)
        return scores[0].url, scores[0]
    
    def find_shortest(self, qurl):
        results = self.find_all(qurl)
        shortest = min([x[1] for x in results], key=lambda y: len(y.url))
        if shortest:
            return shortest.url, shortest
        else:
            return None, None
        
    def find_longest(self, qurl):
        results = self.find_all(qurl)
        longest = max([x[1] for x in results], key=lambda y: len(y.url))
        if longest:
            return longest.url, longest
        else:
            return None, None
        
class TSTRouter(URLRouter):
    
    def __init__(self):
        """First, design implementions for TSTree, DoubleLinkedList, BSTree, Dictionary and Python dict. Then, see which functions are common to all and move to this superclass"""
        self.urls = TSTree()
        
    def add(self, url, action):
        unode = URLRouterNode(url, action)
        self.urls.set(url, unode)
        
    def find_exact(self, url):
        result = self.urls.get(url)
        return result.url, result
        
    def find_all(self, url):
        return sorted(self.urls.find_all(url), key=lambda x: x[1].url)

class DLLRouter(URLRouter):
    
    def __init__(self):
        """First, design implementions for TSTree, DoubleLinkedList, BSTree, Dictionary and Python dict. Then, see which functions are common to all and move to this superclass"""
        self.urls = DoubleLinkedList()
        
    def add(self, url, action):
        # make this a sorted insert (like insort).
        unode = URLRouterNode(url, action)
        if self.urls.begin == None:
            # print("FIRST ADD")
            self.urls.push(unode)
        else:
        # read all contents to python list. sort list by key. copy sorted contents back to nodes.
            self.urls.push(unode)
            # copy all existing url node values to list
            node = self.urls.begin
            key_vals = []
            while node:
                key_vals.append((node.value.url, node.value.action))
                node = node.nxt
            key_vals.sort(key=lambda x: x[0])
            node = self.urls.begin
            for un in key_vals:
                # overwrite the url nodes with the sorted values
                node.value.url = un[0]
                node.value.action = un[1]
                node = node.nxt
                
    def _binary_search(self, qurl, lo=None, hi=None):
        """ binary search algorithm specifically for urlNodes in double linked list."""
        if hi == None:
            lo = 0
            hi = self.urls.count()
        while hi > lo:
            middle = ((hi - lo) // 2) + lo
            mid_node = self.urls.get(middle)
            # print(f"value = {value}, d[{middle}] = {mid_val}")
            if qurl == mid_node.url:
                # print(":::case1")
                return mid_node
            elif  qurl < mid_node.url:
                # print(">>>case2")
                hi = middle
            elif qurl > mid_node.url:
                # print("***case3")
                lo = middle
            if hi-lo <= 1:
                break
        return None

    def _dll_node_list(self, node):
        """diagnostic method"""
        while node:
            # print(node.value.url, node.value.action)
            # node = node.nxt
            print(node.value.url, "\n")
            node = node.nxt

    def find_exact(self, qurl):
        unode = self._binary_search(qurl)
        if unode:
            return unode.url, unode
        return None
        
    def find_all(self, qurl):
        node = self.urls.begin
        qurl_length = len(qurl)
        results = []
        while node:
            if qurl_length <= len(node.value.url):
                if qurl == node.value.url[:qurl_length]:
                    results.append((node.value.url, node.value))
            node = node.nxt
        return sorted(results, key=lambda x: x[1].url)

class BSTRouter(URLRouter):
    
    def __init__(self):
        """First, design implementions for TSTree, DoubleLinkedList, BSTree, Dictionary and Python dict. Then, see which functions are common to all and move this to superclass"""
        self.urls = BSTree()
        
    def add(self, url, action):
        unode = URLRouterNode(url, action)
        self.urls.set(url, unode)

    def find_exact(self, url):
        return url, self.urls.get(url)

    def find_all(self, qurl, node=None, results=None):
        if node == None:
            if not self.urls.root:
                return None, None
            else:
                node = self.urls.root
                results = []
        qurl_len = len(qurl)
        if node:
            node_len = len(node.value.url)
            if qurl_len <= node_len and qurl == node.value.url[:qurl_len]:
                # print("qurl_len <= node_len and qurl == node.value.url[:qurl_len]")
                results.append((node.value.url, node.value))
                # print("results: ", results)
                if node.left:
                    self.find_all(qurl, node.left, results)
                if node.right:
                    self.find_all(qurl, node.right, results)
            elif qurl < node.value.url:
                # print("qurl < node.value.url")
                if node.left:
                    self.find_all(qurl, node.left, results)
            elif qurl > node.value.url:
                # print("qurl > node.value.url")
                if node.right:
                    self.find_all(qurl, node.right, results)
        return sorted(results, key=lambda x: x[1].url)
    
class DictRouter(URLRouter):
    
    def __init__(self):
        """First, design implementions for TSTree, DoubleLinkedList, BSTree, Dictionary and Python dict. Then, see which functions are common to all and move this to superclass"""
        self.urls = Dictionary()
        
    def add(self, url, action):
        unode = URLRouterNode(url, action)
        self.urls.set(url, unode)

    def find_exact(self, qurl):
        return qurl, self.urls.get(qurl)

    def find_all(self, qurl, node=None):
        if not self.urls.map.begin:
            return None, None
        else:
            results = []
            qurl_length = len(qurl)
            bucket = self.urls.map.begin
        while bucket:
            slot = bucket.value.begin
            while slot:
                # print(slot.value)
                slot_length = len(slot.value[1].url)
                if qurl_length <= slot_length and qurl == slot.value[1].url[:qurl_length]:
                    # print("SLOT VALUE: ", slot.value[1].url)
                    results.append((slot.value[1].url, slot.value[1]))
                slot = slot.nxt
            bucket = bucket.nxt
        return sorted(results, key=lambda x: x[1].url)
                    
class PyDictRouter(URLRouter):
    
    def __init__(self):
        """First, design implementions for TSTree, DoubleLinkedList, BSTree, Dictionary and Python dict. Then, see which functions are common to all and move to this superclass"""
        self.urls = dict()
        
    def add(self, url, action):
        unode = URLRouterNode(url, action)
        self.urls[url] = unode

    def find_exact(self, qurl):
        result = self.urls.get(qurl, None)
        return result and (qurl, result) or (None, None)
        
    def find_all(self, qurl):
        qurl_length = len(qurl)
        results = []
        for key in self.urls.keys():
            if qurl_length <= len(key) and qurl == key[:qurl_length]:
                results.append((key, self.urls[key]))
        return sorted(results, key=lambda x: x[1].url)


if __name__=="__main__":
    def routest(which_router, heading):
        print(heading)
        urlroute = which_router()
        urlroute.add("/go/to/sleep/", "sleepy")
        urlroute.add("/give/it/up/before/you/live/it/up/", "humble")
        urlroute.add("/troubadour/blues/", "folk revival")
        urlroute.add("/octopus/is/in/trouble/", "rescue warrior")
        urlroute.add("/go/give/five/dollars/", "beg for pardon")
        urlroute.add("/cant/leave/well/enough/alone/", "spontaneous greed")
        urlroute.add("/gifted/and/exceptional/", "labels")
        print("find_exact '/troubadour/blues':", urlroute.find_exact("/troubadour/blues/"))
        print("***find_all('/')")
        for x in urlroute.find_all("/"):
            print(x)
        print("find_shortest: ", urlroute.find_shortest("/go/"))
        print("find_longest:", urlroute.find_longest("/go/"))

    routest(BSTRouter, "***BSTRouter***:")
    routest(TSTRouter, "***TSTRouter***:")
    routest(DLLRouter, "***DLLRouter***:")
    routest(DictRouter, "***DictRouter***:")
    routest(PyDictRouter, "***PyDictRouter***:")