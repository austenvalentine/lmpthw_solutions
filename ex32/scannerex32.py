import re

class Token(object):
    def __init__(self, token, lexeme, end):
        self.token = token
        self.lexeme = lexeme
        self.end = end
    
    def __repr__(self):
        return f"{self.token}, '{self.lexeme}', {self.end}"
        

class Scanner(object):
    def __init__(self, code, tokens):
        self.code = [line.rstrip() for line in code]
        self.tokens = {}
        for regex, token in tokens:
            self.tokens[token] = re.compile(regex)
        self.script = []
    
    def scan(self):
        for line in self.code:
            i = 0
            while i <= len(line):
                token = self.match_scan(line, i)
                if token:
                    self.script.append(token)
                    i += len(token.lexeme)
                else:
                    i += 1
        return self.script
    
    def match_scan(self, string, index):
        substring = string[index:]
        for tok in self.tokens:
            m = self.tokens[tok].match(substring)
            if m:
                substart, subend = m.span()
                token, substring, end = tok, substring[substart:subend], index + subend
                result = Token(token, substring, end)
                return result
            else:
                continue
        return None
        
    def match_script(self, token_type):
        if self.script:
            if token_type == self.script[0].token:
                return self.script.pop(0)
        return None
            
                
    def peek(self, match_token_type):
        """ returns a list of indices where the token type is matched in the self.script list"""
        results = []
        if self.script:
            i = 0
            while i < len(self.script):
                if match_token_type == self.script[i].token:
                    results.append(i)
                i += 1
        return results

            
    def push(self, psh_tok):
            self.script.insert(0, psh_tok)
            
    def skip(self, skip_tok):
        if self.script:
            while skip_tok == self.script[0].token:
                self.match_script(skip_token)
        return None