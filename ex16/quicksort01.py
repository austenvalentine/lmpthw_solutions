from dllist import *
from random import shuffle

def qsrot(A):
    quicksort(A, 0, A.count() - 1)
    
def refer_node(A, idx):
    """return node object which corresponds with index parameter"""
    acount = A.count()
    if idx > acount - 1:
        return None
    i = 0
    node = A.begin
    while i < idx:
        node = node.nxt
        i += 1
    return node
        
    
def quicksort(A, lo, hi):
    if lo < hi:
        p = partition(A, lo, hi)
        quicksort(A, lo, p - 1)
        quicksort(A, p + 1, hi)
        
def partition(A, lo, hi):
    pivot = A.get(hi)
    i = lo - 1
    j = lo
    jnode = refer_node(A, j)
    inode = jnode.prev
    hinode = refer_node(A, hi)
    while j < hi - 1:
        if A.get(j) < pivot:
            i = i + 1
            inode = inode.nxt
            # SWAP node values
            # A[i] with A[j]
            inode.value, jnode.value = jnode.value, inode.value
        j += 1
        jnode = jnode.nxt
            
    if A.get(hi) < a.get(i + 1):
        # SWAP
        # A[i + 1] with A[hi]
        inode.nxt.value,hinode.value = hinode.value, inodex.nxt.value
    return i + 1

# VVV TEST refer_node HERE VVV
h = DoubleLinkedList()
g = list(range(6, 16))
shuffle(g)
for f in g:
    h.push(f)
    
for i in range(h.count()):
    print(f"node h[{i}]: ", refer_node(h, i))
    
qsrot(h)
print(">>>qsrot(h)")
for i in range(h.count()):
    print(f"h[{i}]: ", h.get(i))