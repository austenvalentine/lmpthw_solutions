from dllist import *

def bubba_srot(nums):
    if nums.begin:
        n = nums.begin.nxt
    else:
        # sort is False if the list is empty
        return False
        
    while True:
        is_sorted = True
        while n:
            if n.value < n.prev.value:
                is_sorted = False
                n.value, n.prev.value = n.prev.value, n.value
            n = n.nxt
        n = nums.begin and nums.begin.nxt or None
        if is_sorted: break
    # assuming successful sort, should only return True
    return is_sorted
    
def cocktail_srot(nums):
    if nums.begin:
        forward = nums.begin.nxt
        backward = nums.end.prev
    else:
        return False
    
    while True:
        # print(">>>main_while")
        is_sorted = True
        while forward:
            # print("+++forward_while")
            if forward.value < forward.prev.value:
                forward.value, forward.prev.value = forward.prev.value, forward.value
                is_sorted = False
            forward = forward.nxt
        forward = nums.begin and nums.begin.nxt or None
        
        while backward:
            # print("===backward_while")
            if backward.value > backward.nxt.value:
                backward.value, backward.nxt.value = backward.nxt.value, backward.value
                is_sorted = False
            backward = backward.prev
        backward = nums.end and nums.end.prev or None
        if is_sorted: break
    return is_sorted
                
    
reverse_srot = DoubleLinkedList()
reverse_srot.push(5)
reverse_srot.push(4)
reverse_srot.push(3)
reverse_srot.push(2)
reverse_srot.push(1)

cocktail_srot(reverse_srot)

print("reverse_srot:")
for i in range(0, 5):
    print(f"reverse_srot({i})", reverse_srot.get(i))

already_srot = DoubleLinkedList()
already_srot.push(1)
already_srot.push(2)
already_srot.push(3)
already_srot.push(4)
already_srot.push(5)

cocktail_srot(already_srot)

print("\n>>>already_srot:")
for i in range(0, 5):
    print(f"already_srot({i})", already_srot.get(i))

dupes = DoubleLinkedList()
dupes.push(1)
dupes.push(3)
dupes.push(5)
dupes.push(1)
dupes.push(3)

cocktail_srot(dupes)

print("\n>>>dupes:")
for i in range(0, 5):
    print(f"dupes({i})", dupes.get(i))

randomes = DoubleLinkedList()
randomes.push(4)
randomes.push(1)
randomes.push(5)
randomes.push(3)
randomes.push(2)

cocktail_srot(randomes)

print("\n>>>randomes:")
for i in range(0, 5):
    print(f"randomes({i})", randomes.get(i))
