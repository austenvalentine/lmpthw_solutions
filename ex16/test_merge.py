from merge import *
from dllist import *
def test_merge_sort():
    h = DoubleLinkedList()
    h.push(13)
    h.push(5)
    h.push(10)
    h.push(1)
    h.push(11)
    h.push(3)
    h.push(7)
    h.push(6)
    h.push(2)
    h.push(9)
    h.push(12)
    h.push(8)
    h.push(4)
    
    # for i in range(4):
    #     print("h.count(): ", h.count())
    #     print("h.begin", h.begin)
    #     print("h.end", h.end)
    #     print("h.unshift(): ", h.unshift())
    
    for i in range(0, h.count()):
        print(f"h.get({i}): {h.get(i)}")
        
    
    # VVV THIS PART IS THE TEST VVV
    h = merge_sort(h)

    print("    >>>TESTING merge_sort: ")
    prev_value = 0
    for i in range(0, h.count()):
        assert prev_value <= h.get(i)
        print(f"h.get({i}): {h.get(i)}")
        prev_value = h.get(i)