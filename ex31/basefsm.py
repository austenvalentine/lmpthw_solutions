class baseFSM(object):
    
    def handler(self, event):
        # print(f"HANDLER: {self.state_name}, EVENT: {event}")
        try:
            self.state_name = getattr(self, self.state_name)(event)
        except AttributeError:
            print("attribute, ", self.state_name, "does not exist")
                
    def logger(self, event):
        pass