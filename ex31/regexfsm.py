import basefsm

class regexFSM(basefsm.baseFSM):
    """this is a finite state machine implementation of the first regular expression on page 153 of Learn More Python The Hard Way: '.*BC?$' """
    
    def __init__(self):
        self.state_name = 'FIRST'
        self.matched = False
        
    def match(self,word):
        self.matched = False
        self.state_name = 'FIRST'
        self.word = word
        self.broke_word = list(word)
        self.broke_word.append('ENDWORD')
        while True:
            if self.matched == True:
                return self.matched
            elif self.broke_word == []:
                return self.matched
            else:
                for ch in self.broke_word:
                    # print(self.broke_word)
                    self.handler(ch)
                self.broke_word = self.broke_word[1:]
    
    def FIRST(self, ch):
        self.matched = False
        return 'SECOND'
    
    def SECOND(self, ch):
        print(">>> SECOND!", ch)
        self.matched = False
        if ch == 'B':
            return 'THIRD'
        if ch == 'ENDWORD':
            return 'THIRD'
        else:
            return 'SECOND'
        
    def THIRD(self, ch):
        print(">>> THIRD!", ch)
        self.matched = False
        if ch == 'C':
            self.matched = True
            return 'THIRD'
        elif ch == 'ENDWORD':
            self.matched = True
            return 'FIRST'
        else:
            return 'FIRST'