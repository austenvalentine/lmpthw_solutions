#!/usr/bin/env/ python3
import sys

# input processing
def take_bytes(dump_in, offset):
    """grab 16 or remaining bytes starting from offset and calculuate new offset"""
    remaining = len(dump_in) - offset
    if remaining > 16:
        line = dump_in[offset:offset + 16]
        new_offset = len(dump_in[offset:offset + 16])
    else:
        line = dump_in[offset:offset + remaining]
        new_offset = dump_in[offset:offset + remaining]
    return line, new_offset

# display functions
def default_display(line_in)):
    line_out = line_in
    return line_out
    
def canonical(line_in)):
    line_out = line_in
    return line_out
    
def twobyte_dec(line_in)):
    line_out = line_in
    return line_out
    
def onebyte_oct(line_in)):
    line_out = line_in
    return line_out
    
def onebyte_char(line_in)):
    line_out = line_in
    return line_out
    
def twobyte_oct(line_in)):
    line_out = line_in
    return line_out
    
    
# output processing
def process_offset_display(offset)
    """return zero-filled 10 char hex value"""
    return hex(offset)[2:].zfill(10)
    

def process_byte_display(line_in, offset, input_offset_display):
    """apply all displays to input"""
    for display in displays:
        

#
# parseable arguments
# no support for format strings
#
display_funcs = {
                "-C" : canonical,
                "-d" : twobyte_dec,
                "-b" : onebyte_oct,
                "-c" : onebyte_char,
                "-o" : twobyte_oct,
                "--default-display" : default_display
                }
                
flags = {}

args = sys.argv[1:]
if len(args) == 0 or args[0] == "--help" or args[0] == "-h":
    print("...put usage instruction here...")
    exit(0)


# output displays may be rendered multiple times per input line
# hence the list
displays = []
files = []
feeds = []

i = 0
while i < len(args):
    if args[i] in display_funcs.keys():
        displays.append(args[i])
        i += 1
    elif args[i] in flags.keys():
        flags[args[i]] = True
        i += 1
    else:
        # bad argument
        files = args[i:]
        
if not displays:
    displays.append("--default-display")

#
# collect feeds from input sources (files or stdin)
#
if files:
    for file in files:
        with fin as open(file):
            feeds.append(fin.read())
else:
    feeds.append(sys.stdin.read())

#
# process input!
#
offset = 0
dumpout = ''

for feed in feeds:
    # grab 16 bytes (or remaining) and reposition offset
    line_in, offset = take_bytes(feed, offset)
    # render input offset as zero-padded, 10-char hex display
    input_offset_display = process_offset_display(offset)
    dump_out += process_byte_display(line_in, offset, input_offset_display)
    
print(dumpout)


