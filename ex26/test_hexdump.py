import subprocess

file_name = 'fileone'

def test_hexdump_canon(file_name=file_name):
    dump_out = subprocess.check_output(('hexdump', '-C', file_name))
    pydump_out = subprocess.check_output(('python', 'hexdump.py', '-C', file_name))
    assert pydump_out == dump_out
    
def test_hexdump_onebyte_char(file_name=file_name):
    dump_out = subprocess.check_output(('hexdump', '-c', file_name))
    pydump_out = subprocess.check_output(('python', 'hexdump.py', '-c', file_name))
    assert pydump_out == dump_out
    
def test_hexdump_onebyte_oct():
    dump_out = subprocess.check_output(('hexdump', '-b', file_name))
    pydump_out = subprocess.check_output(('python', 'hexdump.py', '-b', file_name))
    assert pydump_out == dump_out
    
if __name__=="__main__":
    test_hexdump_canon()