import re, argparse

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

parser = argparse.ArgumentParser()
parser.add_argument("-n", action="store_true")
parser.add_argument("PATTERN", action='store')
parser.add_argument("FILES", nargs='*')
args = parser.parse_args()

print(args)

for file in args.FILES:
    fin = open(file)
    if args.n == True:
        line_count = 1
    for line in fin.readlines():
        # learn to do this with re module
        if args.PATTERN in line:
            line = bcolors.HEADER + line
            # format line numbers
            if args.n == True:
                line = ':' + bcolors.OKBLUE + str(line_count) + ':' + line
                line_count += 1
            line = bcolors.OKGREEN + file + line
            print(line, end='')
            # reset terminal colours
            print('\033[0m', end='')
    fin.close()

