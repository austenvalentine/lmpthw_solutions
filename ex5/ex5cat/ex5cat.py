import argparse

parser = argparse.ArgumentParser(description='concatenate input')
parser.add_argument('-n', '--number', action='store_true')
parser.add_argument('files', nargs=argparse.REMAINDER)
args = parser.parse_args()


def catout(filename):
    try:
        fin = open(filename, 'r')
        count = 1
        for line in fin.readlines():
            output = line
            print(">>>args.number = ", args.number)
            if args.number:
                output = "\t" + count + line
                count += 1
            print(output, end='')
        fin.close()
    except:
        fin(close)
        print('ex5cat:', filename, ": No such file or directory")
        print(output)
        print(args.number)
        exit(1)

for file in args.files:
    catout(file)

exit(0)