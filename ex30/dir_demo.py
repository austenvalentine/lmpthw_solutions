class parent(object):
    def __init__(self):
        self.x = 'parent attribute'
    
    def p_func(self):
        print('this is the pfunc')
        
    def scan_all(self):
        print(dir(self))
        
class child(parent):
    def __init__(self):
        parent.__init__(self)
        self.y = 'child attribute'
        
    def c_func(self):
        print('can you see me?')
        

        
c = child()
c.scan_all()
c.p_func()
c.c_func()