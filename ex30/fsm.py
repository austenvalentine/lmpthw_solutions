class FSM(object):
    def __init__(self):
        # self.START()
        pass
    
    def handler(self, event):
        if hasattr(self, self.state_name):
            self.state_name = getattr(self, self.state_name)(event)
        else:
            print(f"Homey don't play {self.state_name}({event}).")
            exit()