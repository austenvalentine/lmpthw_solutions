import ex30fsm

def test_basic_connection():
    machine = ex30fsm.Fsm()
    state = machine.START()
    script = ["connect", "accept", "read", "read", "write", "close", "connect"]
    
    for event in script:
        print(event, ">>>", state)
        state = state(event)
        
if __name__=="__main__":
    test_basic_connection()