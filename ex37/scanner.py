import re
class Token(object):
    def __init__(self, lineno, label, lexeme, index):
        self.lineno = lineno
        self.label = label
        self.lxm = lexeme
        self.idx = index

    def __repr__(self):
        return f"<{self.lineno} {self.label}, lxm:'{self.lxm}', idx:{self.idx}>"

class Scanner(object):
    def __init__(self, PATTERNS, CODE):
        # split at newlines, but tack them back on to the end of each line
        self.code = [line.upper() + "\n" for line in CODE.split("\n")]
        self.code_idx = 0
        self.regexs = self.compile_patterns(PATTERNS)
        self.tokens = []
        self.scan()

    def compile_patterns(self, patterns):
        compiled = []
        for pattern in patterns:
            compiled.append((re.compile(pattern[0]), pattern[1]))
        return compiled
    
    def scan(self):
        for line in self.code:
            self.match_statement(line)

    def match_statement(self, line):
        num_match = re.match("^[0-9]+\s*", line)
        lineno = int(num_match.group())
        assert lineno
        self.code_idx += len(num_match.group())
        line_idx = len(num_match.group())
        while len(line[line_idx:]) > 0:
            for (regex, label) in self.regexs:
                result = regex.match(line[line_idx:])
                if result:
                    idx = self.code_idx
                    lxm = result.group()
                    self.code_idx += len(lxm)
                    line_idx += len(lxm)
                    lxm = lxm.rstrip()
                    token = Token(lineno, label, lxm, idx)
                    self.tokens.append(token) 
                result = None
