from pytr import *
import subprocess

def test_default_sub():
    arg1 = 'a\n'
    arg2 = ' '
    with open("output_filetwo") as fin:
        tr_out = fin.read()
    with open("filetwo") as fin:
        tr_in = fin.read()
    assert default_sub(arg1, arg2, tr_in) == tr_out
    
def test_delete_chars():
    tr_out = subprocess.getoutput(["tr -d 'ab\n' < filetwo"])
    pytr_out = subprocess.getoutput(["python pytr.py -d 'ab\n' < filetwo"])
    assert tr_out == pytr_out