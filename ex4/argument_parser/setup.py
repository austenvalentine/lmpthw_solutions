#!/usr/bin/env python3
from distutils import setup
params = {
    name='Argumentative Demonstration,'
    version='0.1',
    description='test of argparser module',
    author='Austen Valentine',
    author_email='aus@austenvalentine.com',
    url='http://www.austenvalentine.com/',
    packages=['ex4_argparse',]
    scripts=[]
    }

setup(*params)
