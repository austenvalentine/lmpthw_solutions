import argparse

parser = argparse.ArgumentParser(description='pargsnipper')

# parser.add_argument('--help')
# parser.add_argument('-h')
parser.add_argument('-e', help='-e help')
parser.add_argument('-f', help='-f help', action='store_true')
parser.add_argument('-g', help='-g help', action='store_true')
parser.add_argument('--flag1', help='flag1 help')
parser.add_argument('--flag2', help='flag2 help', action='store_true')
parser.add_argument('--flag3', help='flag3 help', action='store_true')
parser.add_argument('positionals', nargs=argparse.REMAINDER, help='position1 help')


args = parser.parse_args()

print(args)