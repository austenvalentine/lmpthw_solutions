import re

class Token(object):
    def __init__(self, token_id, lexeme, end):
        self.token_id = token_id
        self.lexeme = lexeme
        self.end = end
        
    def __repr__(self):
        return f"token_id: {self.token_id}, lexeme: '{self.lexeme}'"

class Scanner(object):

    def __init__(self, regex, code):
        self.regex_list = []
        for tup in regex:
            self.regex_list.append((re.compile(tup[0]), tup[1]))
        self.code = code
        self.tokens = self.scan()

    def ignore_ws(self):
        while self.tokens[0].token_id == 'INDENT':
            self.tokens.pop(0)

    def match(self, token_id):
        if token_id != 'INDENT':
            self.ignore_ws()

        if self.tokens[0].token_id == token_id:
            return self.tokens.pop(0)
        else:
            # return ['ERROR', 'error'], token_id
            print("match ERROR: ", token_id, self.tokens[0].token_id)
            return False,

    def peek(self):
        self.ignore_ws()
        return self.tokens[0].token_id

# Examine 'skip' further
    def skip(self, *what):
        for x in what:
            if x != 'INDENT': self.ignore_ws()

            tok = self.tokens[0]
            if tok[0] != x:
                return False
            else:
                self.tokens.pop(0)
        return True


    def match_regex(self, i, line):
        start = line[i:]
        for regex, token in self.regex_list:
            tok = regex.match(start)
            if tok:
                begin, end = tok.span()
                return token, start[:end], end
        return None, start, None

    def scan(self):
        self.script = []
        for line in self.code:
            i = 0
            while i < len(line):
                token_id, string, end = self.match_regex(i, line)
                token = Token(token_id, string, end)
                assert token, "Failed to match line %s" % string
                if token:
                    i += end
                    self.script.append(token)
        return self.script

    def done(self):
        return len(self.tokens) == 0
