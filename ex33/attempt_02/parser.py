# This is my second attempt at the parser
import scanner
import productions

class Parser(object):
    def __init__(self, token_regex, code):
        self.scanner = scanner.Scanner(token_regex, code)
        self.productions = self.parse()
        
    def peek(self):
        return self.scanner.peek()
        
    def match(self, *args):
        return self.scanner.match(*args)
    
    def skip(self, *args):
        return self.scanner.skip(*args)
            
    def parse(self):
        """parse() method drives the parsing"""
        return self.root()
        
            
    def root(self):
        """root = *(funccal / funcdef)"""
        tokens = self.scanner.tokens
        root_body = []
        while tokens:
            token_id = self.peek()
            if token_id == 'DEF':
                root_body.append(self.funcdef())
            elif token_id == 'NAME':
                root_body.append(self.funccall())
            else:
                assert False, f"ERROR: invalid root token {token_id}"
        return productions.Root(root_body)
                
    def funcdef(self):
        """funcdef = DEF name LPAREN params RPAREN COLON body
        """
        assert self.match('DEF')
        name = self.match('NAME')
        self.match('LPAREN')
        params = self.params()
        assert self.match('RPAREN')
        assert self.match('COLON')
        body = self.body()
        return productions.FuncDef(name, params, body)
        
    def params(self):
        """params = expression *(COMMA expression)"""
        parameters = []
        expr = self.expression()
        if expr:
            parameters.append(expr)
        while self.peek() == 'COMMA':
            self.match('COMMA')
            if self.peek() == 'RPAREN':
                break
            else:
                expr = self.expression()
                parameters.append(expr)
        return productions.Params(parameters)
        
    def expression(self):
        """expression = name / PLUS / integer"""
        token = None
        first_type = self.peek()
        if first_type == 'NAME':
            token = self.match('NAME')
            express = productions.ExprVariable(token)
        elif first_type == 'INTEGER':
            token = self.match('INTEGER')
            express = productions.ExprNumber(token)
        else:
            assert False, f"INVALID TOKEN {first_type}"
        second_type = self.peek()
        if second_type == 'PLUS':
            return self.plus(express)
        else:
            return express
        
    def plus(self, left):
        """expression PLUS expression"""
        # check match these assertions - is ok to chain
        # attribute after function call?
        assert self.match('PLUS')
        if self.peek() == 'NAME':
            right = self.expression()
            return productions.ExprPlus(left, right)
    
    def body(self, prev_indent_depth=0):
        # forget this grammar for
        """body: expression *(NEWLINE INDENT expression) DEDENT"""
        # come up with a sensible indent system and productions which record indent/dedent levels
        # indent = self.match(indent)
        body_lines = []
        funccall = self.funccall()
        body_lines.append(funccall)
        return productions.Body(body_lines)
        
    def funccall(self):
        """funccall = name LPAREN params RPAREN"""
        name = self.match('NAME')
        assert self.match('LPAREN')
        params = self.params()
        assert self.match('RPAREN')
        return productions.FuncCall(name, params)