from dllist import *

def to_list(dll):
    print(dll)
    builtin = []
    node = dll.begin
    while node:
        builtin.append(node)
        node = node.nxt
    return builtin
    
    
def refer_node(A, idx):
    """return node object which corresponds with index parameter"""
    # Performance: This gets called a hell of a lot.
    # The cumulative penalty is pretty bad. Try rebuilding the
    # count() method.
    acount = A.count()
    if idx > acount - 1:
        return None
    i = 0
    node = A.begin
    while i < idx:
        node = node.nxt
        i += 1
    return node


def bubble_sort(nums):
    """sort element values in ascending order"""
    # Performance: This is  a slow algorithm. There isn't much
    # that can be done here
    while True:
        ordered = True
        n = nums.begin
        n1 = n.nxt
        while n1:
            if n.value > n1.value:
                n.value, n1.value = n1.value, n.value
                ordered = False
            n = n1
            n1 = n1.nxt
        if ordered: break
    
def value_list(A):
    B = list()
    node = A.begin
    while node:
        B.append(node.value)
        node = node.nxt
    return B
    
def to_DLL(A, DLL):
    assert len(A) == DLL.count()
    node = DLL.begin
    i = 0
    while node:
        node.value = A[i]
        node = node.nxt
        i += 1
    return
        
def msort(A):
    # input and output lists
    h = value_list(A)
    h = merge_sort(h)
    # copy result values back to double-linked list
    to_DLL(h, A)
    
def merge_sort(A):
    # Performance: this gets called a lot (recursive).
    alength = len(A)
    # Base case. A list of zero or one elements is sorted, by definition.
    if alength <= 1:
        return A
    # Recursive case. First, divide the list into equal-sized sublists
    # consisting of the first half and second half of the list.
    # This assumes lists start at index 0.
    halfa = alength // 2
    left = A[0:halfa]
    right = A[halfa:alength]

    # Recursively sort both sides
    left = merge_sort(left)
    right = merge_sort(right)
    
    # Then merge the now-sorted sublists
    return merge(left, right)

def merge(left, right):
    result = list()
    
    while left and right:
        if left[0] <= right[0]:
            result.append(left.pop(0))
        else:
            result.append(right.pop(0))
            
        # Either left or right may have elements left; consume them
        # print("left.begin", left.begin)
        # print("right.begin", right.begin)
        # (Only one of the following loops will actually be entered)
    while left:
        result.append(left.pop(0))
    while right:
        result.append(right.pop(0))
            
    return result

    
def quicksort(A, lo, hi):
    if lo < hi:
        p = partition(A, lo, hi)
        quicksort(A, lo, p - 1)
        quicksort(A, p + 1, hi)
        
        
def partition(A, lo, hi):
    pivot = A[hi]
    i = lo - 1
    j = lo
    inode = None
    while j <= hi - 1:
        jnode = A[j]
        if jnode.value < pivot.value:
            i += 1
            inode = A[i]
            jnode.value, inode.value = inode.value, jnode.value
        j += 1
    iplusnode = A[i + 1]
    hinode = A[hi]
    if hinode.value < iplusnode.value:
        hinode.value, iplusnode.value = iplusnode.value, hinode.value
    return i + 1
    
    
def qsort(A):
    h = to_list(A)
    quicksort(h, 0, A.count() - 1)
    
    
if __name__ == "__main__":
    d = DoubleLinkedList()
    d.push(31)
    d.push(2)
    d.push(5)
    node = d.begin
    i = 0
    while node:
        print(f"d[{i}]:", node.value)
        node = node.nxt
    print("msort(d)")
    msort(d)
    node = d.begin
    i = 0
    while node:
        print(f"d[{i}]:", node.value)
        i += 1
        node = node.nxt