import parser
class WorldStack(object):
    """ keep the World instances of a given scope in a list """
    def __init__(self):
        self.stack = []

    def push_world(self):
        pass

    def pop_world(self):
        pass

    def push_child(self):
        pass

    def pop_child(self):
        pass

class World(object):
    def __init__(self, parent=None):
        self.parent = parent
        self.symbol_table = dict()
        self.functions = dict()
        self.children = dict() 

    def declare_symbol(self, name):
        self.symbol_table[name] = None
        # print("SYMBOL DECLARED: ", self.symbol_table.keys())

    def lookup_symbol(self, name):
        value = self.symbol_table.get(name, None)
        if value:
            return value
        else:
            try:
                return self.parent.lookup_symbol(name)
            except:
                assert False, f"SYMBOL LOOKUP FAIL: {name}"

    def declare_function(self, function_def):
        name = function_def.name
        self.functions[name] = function_def
        # print("FUNCTION DECLARED: ", self.functions.keys())

    def lookup_func(self, name):
        function = self.functions.get(name, None)
        if function:
            return function
        else:
            try:
                return self.parent.lookup_func(name)
            except:
                assert False, f"FUNCTION LOOKUP FAIL: {name}"

    def spawn(self, func_name):
        """Allows World class to implement local scope in func_def visitor method"""
        child = World(parent=self)
        self.children[func_name] = child
        return child 

    def lookup_symbol(self, name):
        try:
            self.symbol_table.get(name)
        except:
            assert False, f"ANALYSIS FAIL: INVALID SYMBOL {name}"

    def __repr__(self):
        rep = f"symbols:{self.symbol_table.keys()}, funcs:{self.functions.keys()}, children{self.children}"
        return f"\n<scope>{rep}\n</scope>"

class Analyzer(object):
    def __init__(self, token_map, code):
        self.parser = parser.Parser(token_map, code)
        self.parser.parse()
        self.ast = self.parser.ast

    def analyze(self):
        world = World()
        self.ast.visit(world)
        self.world = world

