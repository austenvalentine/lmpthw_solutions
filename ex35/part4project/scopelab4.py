#!/usr/bin/env python3

# This script tests lexical scope where a function, 'env_func', recursively
# stores a function object, 'free_func', in an accumulator. 'free_func' prints a
# free variable, 'x'.

# recursive function for enclosing scope
def env_func(x, acc):
    if x == 1:
        print("BASE free_func: ")
        free_func()
        return
    else:
        def free_func():
            # 'x' is the free variable within free_func
            print(f"free x == {x}")
        # push definition onto accumulator
        acc.append(free_func)
        env_func(x-1, acc)

free_func_accumulator = list()
env_func(5, free_func_accumulator)

# call each definition and display value of free variable 'x'
for i in range(len(free_func_accumulator)):
    print(f"free_func_accumulator[{i}](): " )
    free_func_accumulator[i]()

