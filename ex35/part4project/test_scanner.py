import scanner

TOKEN_MAP = [ 
        ("DEF", r"^def\s"),
        ("NAME", r"^[_A-Za-z]+[_0-9A-Za-z]*\s*"),
        ("LPAREN", r"^\(\s*"),
        ("INTEGER", r"^[0-9]+\s*"),
        ("COMMA", r"^,\s*"),
        ("RPAREN", r"^\)\s*"),
        ("COLON", r"^:\s*"),
        ("PLUS", r"^\+\s*"),
        ("INDENT", r"^\s+"),
        ] 

CODE_SAMPLE = """def hello(x, y):
    print(x,y)

hello(1, 2)
hello(3, 4)
"""

if __name__=="__main__":
    code = CODE_SAMPLE.split('\n')
    print(">>>CODE:\n")
    for line in code:
        print(line)
    tokenizer = scanner.Scanner(TOKEN_MAP)
    tokenizer.scan(code)
    print(tokenizer.tokenized)
