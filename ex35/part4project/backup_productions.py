class Production(object): pass

class Statement(Production):
    def __init__(self, assign_id, expression):
        self.assign_id = assign_id
        self.expression = expression 

    def __repr__(self):
        return f"\n<Statement>{self.assign_id}{self.expression}\n</Statement>"

    def visit(self, world):
        if self.assign_id:
            self.assign_id.visit(world)
        if self.expression:
            self.expression.visit(world)

class AssignID(Production):
    def __init__(self, name):
        self.name = name

    def visit(self, world):
        world.declare_symbol(self.name)

    def __repr__(self):
        return f"\n<AssignID name:'{self.name}'></AssignID>"

class Expr(Production):
    def __repr__(self):
        return f"\n<Expr '{self.value}'></EXPR>"

class ExprInt(Expr):
    def __init__(self, value):
        self.value = value

    def visit(self, world):
        assert type(self.value) == int

    def ret(self):
        return self.value

    def __repr__(self):
        return f"\n<ExprInt val:'{self.value}'></ExprInt>"

class ExprID(Expr):
    def __init__(self, name):
        self.name = name

    def visit(self, world):
        assert self.name in world.symbol_table

    def ret(self, stack_frame):
        return stack_frame.lookup(self.name)

    def __repr__(self):
        return f"\n<ExprID name:'{self.name}'></ExprID>"

class ExprPlus(Expr):
    def __init__(self, left, right):
        self.left_prod = left
        self.right_prod = right

    def visit(self, world):
        # unfortunately, there's no simple way to check that 
        # left and right operands are of the same type if either
        # one turns out to be a function call
        self.left_prod.visit(world)
        self.right_prod.visit(world)

    def ret(self):
        left_value = self.left_prod.ret()
        right_value = self.right_prod.ret()
        assert type(left_value) == type(right_value), "Unmatched operand types"
        return left_value + right_value

    def __repr__(self):
        return f"\n<ExprPlus>{self.left_prod}{self.right_prod}\n</ExprPlus>"

class FuncCall(Expr):
    def __init__(self, name, params):
        self.name = name
        self.params = params

    def visit(self, world):
        assert world.lookup_func(self.name)
        for elt in self.params:
            elt.visit(world)

    def __repr__(self):
        return f"\n<FuncCall {self.name}>{self.params}\n</FuncCall>"

class FArg(object):
    def __init__(self, name):
        self.name = name 

    def visit(self, world):
        world.declare_symbol(self.name)

    def ret(self):
        pass
    
    def __repr__(self):
        return f"\n<FArg name:'{self.name}'></FArg>"

class FuncDef(object):
    def __init__(self, name, formal_args, body):
        self.name = name
        self.formal_args = formal_args
        self.body = body

    def visit(self, parent_world):
        local_world = parent_world.spawn(self.name)
        parent_world.declare_function(self)
        for elt in self.formal_args:
            elt.visit(local_world)
        self.body.visit(local_world)

    def __repr__(self):
        return f"\n<FuncDef name:{self.name}>{self.formal_args}{self.body}\n</FuncDef>"

class BIPrint(object):
    """ a built-in print function at the root level """
    def __init__(self):
        self.name = 'print'
        self.formal_args = [FArg('prtarg')]

    def visit(self, parent_world):
        local_world = parent_world.spawn(self.name)
        parent_world.declare_function(self)
        for elt in self.formal_args:
            elt.visit(local_world)
    
    def call(self, stack_frame):
        """ maybe use the python __call__ method instead """
        prtarg = stack_frame.get_value('prtarg')
        print(prtarg)
        return None

    def __repr__(self):
        return f"\n<BIPrint>{self.formal_args}\n</BIPrint>"

class Body(object):
    """ *(expr | funcdef) """
    def __init__(self, indent_level, contents):
        self.indent_level = indent_level
        self.contents = contents 

    def visit(self, world):
        for elt in self.contents:
            elt.visit(world)

    def __repr__(self):
        return f"\n<Body indt:{self.indent_level}>{self.contents}\n</Body indt:{self.indent_level}>"

class Root(Body):
    def __init__(self, indent_level, contents): 
        self.indent_level = indent_level
        builtins = self.get_builtins()
        self.contents = builtins + contents

    def get_builtins(self):
        builtins = [BIPrint(), ]
        return builtins

    def visit(self, world):
        for elt in self.contents:
            elt.visit(world)

    def __repr__(self):
        return f"\n<Root indt:{self.indent_level}>{self.contents}\n</Root indt:{self.indent_level}>"
    
