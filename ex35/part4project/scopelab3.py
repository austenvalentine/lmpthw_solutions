#!/usr/bin/env python3

# this test illustrates how free variables point to their insertions outside
# their immediate scope rather than copying the values from in the parent into
# their own local symbol table

def caller():
    x = "caller's x BEFORE def hello"
    def hello():
        print(">>>hello x: ", x)
    print("CALLER:hello()")
    hello()
    x = "caller's x AFTER def hello"
    return hello

print("GLOBAL: glo_hello = caller()")
glo_hello = caller()
print("GLOBAL: glo_hello()")
glo_hello()
