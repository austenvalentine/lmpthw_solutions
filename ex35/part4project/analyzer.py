import parser

class NonValue(object):
    """NonValue is recorded in the symbol table upon variable initialization. Unlike Python's None, NonValue is only a value placeholder in the symbol table and
    cannot be used to evaluate expressions. This is so that this QuasiPy language can use the built in None from Python."""
    pass

class LocalScope(object):
    """ Local Scope is used by the semantic analyzer to declare all variables 
    within a local scope, including function definitions. In the symbol table, function code/ast is
    bundled with its respective scope as { function_name : (function, scope) }
    Recursion is handled at the interpreter stage.
    """

    def __init__(self, parent=None):
        """ symbol table contains non-evaluated expressions for variables and
        tuples of (function, scope) where scopes is a LocalScope.
        parent is another LocalScope which is the enclosing environment for
        the self/current LocalScope """
        self.symbol_table = dict()
        self.parent = parent

    def __repr__(self):
        return f"\n<LocalScope>{self.stack}\n\n</LocalScope>"

    def declare_function(self, function, parent):
        child_scope = LocalScope(parent)
        if function.name in self.symbol_table:
            # func_entry is the (function, scope) pair
            func_entry = self.symbol_table[function.name]
            # append child_scope to the function's scopelist, 2nd tuple el't
            func_entry[1] = child_scope
        else:
            # insert new function def entry
            self.symbol_table[function.name] = (function, child_scope)
        return child_scope

    def analyze_id(self, name):
        # used by the AssignID object to insert name into symbol
        # table for analysis stage
        self.symbol_table[name] = NonValue()

    def declare_id(self, name, world):
        # change this so that the world/frame is not referenced by
        # list position, but instead via a reference passed by the caller
        # in case there are multiple/stacked environments in which
        # the current function scope was declared.
        pass

    def lookup_symbol(self, name):
       if name in self.symbol_table:
           return self.symbol_table.get(name, NonValue())
       else:
           try:
               return self.parent.lookup_symbol(name)
           except:
               assert False, f"SYMBOL LOOKUP FAIL: {name}"

    def symbol_exists(self, name):
        if name in self.symbol_table:
            return True
        else:
            try:
                return self.parent.symbol_exists(name)
            except:
                assert False, f"SYMBOL EXISTS FAIL: {name}"

    def __repr__(self):
        rep = f"symbols:{self.symbol_table}"
        return f"\n{rep}\n"

class Analyzer(object):
    def __init__(self, token_map, code):
        self.parser = parser.Parser(token_map, code)
        self.parser.parse()
        self.ast = self.parser.ast

    def analyze(self):
        self.scope = LocalScope()
        self.ast.visit(self.scope)
