#!/usr/bin/env python
import interpreter

TOKEN_MAP = [ 
        ("DEF", r"^def\s"),
        ("RETURN", r"^return\s*"),
        ("NAME", r"^[_A-Za-z]+[_0-9A-Za-z]*\s*"),
        ("LPAREN", r"^\(\s*"),
        ("INTEGER", r"^[0-9]+\s*"),
        ("COMMA", r"^,\s*"),
        ("RPAREN", r"^\)\s*"),
        ("COLON", r"^:\s*"),
        ("PLUS", r"^\+\s*"),
        ("ASSIGN", r"^\=\s*"),
        ("INDENT", r"^\s+"),
        ] 

OLD_CODE_SAMPLE = """def hello(x, y):
    print(x + y)

hello(1, 2)
hello(3, 4)
a = 5
b = 6 + 7
c = a + b
a + b + c
hello(a, b)
goodbye = hello
hello = 0
goodbye(c, c)
"""
# The following sample tests for lexical scope rules - can the interpreter
# get python-like semantics instead of resorting to closure?
CODE_SAMPLE = """def outside():
    local_var = 1
    free_var = 2
    def inside():
        print(local_var)
        local_var = 3
        print(local_var)
        print(free_var)
        local_var = 4
    print(local_var)
    print(free_var)
    inside()
    print(local_var)
    print(free_var)
    local_var = 7
    inside()
outside()
""" 

def main():
    code = CODE_SAMPLE.split('\n')
    print("VVV CODE STARTS BELOW VVV")
    for line in code:
        print(line)
    print("VVV OUTPUT STARTS BELOW VVV")
    terp = interpreter.Interpreter(TOKEN_MAP, code)    
    terp.interpret()
    
if __name__=="__main__":
    main()
