import fileinput, argparse

def pick_fields(preout, fields):
    processed = ''
    for field in fields:
        field = int(field)
        if field <= len(preout):
            processed += preout[field - 1] + ' '
    return processed


parser = argparse.ArgumentParser()
# this argument is for delimiter
parser.add_argument('-d', action="store")
# this argument is for field (column)
parser.add_argument('-f', action="store")
# filenames (if they apply)
parser.add_argument('files', nargs='*', action="store")

args = parser.parse_args()
print(args)

# check for paramaters, otherwise supply default
if args.d:
    DELIM = args.d
else:
    DELIM = '\t'
    
if args.f:
    FIELDS = args.f.split(',')

# find out if there are files listed
if args.files:
    preout = ''
    output = ''
    # open each file and process each line
    for file in args.files:
        fin = open(file)
        for line in fin:
            line = line.strip()
            preout = line.split(DELIM)
            output = pick_fields(preout, FIELDS)
            print(output)
    exit(0)
else:
    preout = ''
    output = ''
    for preout in fileinput.input():
        # preout = preout.strip()
        # output = pick_fields(preout, FIELDS)
        print(preout)
        # print(output)
    